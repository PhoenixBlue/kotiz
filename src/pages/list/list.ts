import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, LoadingController, AlertController, ToastController } from 'ionic-angular';
import { StoreAPI } from '../../providers/store/store';
import { Shared } from '../../shared/shared';
import { TranslateService } from '@ngx-translate/core';


@IonicPage()
@Component({
  selector: 'page-list',
  templateUrl: 'list.html',
})
export class ListPage {
  items:any[] = [];
  selectedItem:any;
  objClass:string;
  tList:any;

  /** CONSTRUCTOR
   * ============= */
  constructor(public navCtrl: NavController,      private modalCtrl: ModalController,
              public loadCtrl: LoadingController, public navParams: NavParams,
              public store:StoreAPI,              public translate:TranslateService,
              private alertCtrl:AlertController,  public toastCtrl: ToastController) {
    this.objClass = this.navParams.get('objClass');
  }

  /** Loading the page
   * ================== */
  ionViewDidLoad() {
    this.loadData()
  }

  /** Add new item in database
   * =========================  */
  addItem(){

    let paramModal = { objClass:this.objClass};
    let modalForm  = this.modalCtrl.create('InputFormPage', paramModal);
    
    modalForm.present();
    modalForm.onWillDismiss((res)=>{

      if(this.objClass.localeCompare(Shared.classSessions)==0 && res.status===true){
        // Selected date for session does not exist, so we can create a new one.
        let argModal   = {objClass: this.objClass, item:res.data, create: true};
        let modalForm2 = this.modalCtrl.create('SessionsPage', argModal);
        const arg2 = {semester:res.data.details.semester.uid, date:res.data.uid};

        let loader = this.loadCtrl.create({
          content: this.translate.instant('INIT_SESSION')
        });
        loader.present().then(()=>{
          // this.store.tempCurrentSession().then(res2=>{
          this.store.initSession(arg2).then(res2=>{
            modalForm2.present();
            modalForm2.onWillDismiss(res3 => {
              if(undefined!==res && res.status===true){
                this.loadData();
              }
            })
          }).catch(err2 => {
            this.showAlert('ERROR', 'FAIL_INIT')
          }).then(() => {loader.dismiss();});
        });

      } else if(undefined!==res && res.status===true){
        this.loadData();
      }
    });
  }

  /** Load list of items
   * ==================== */
  loadData(){
    let loader = this.loadCtrl.create({
      content: this.translate.instant('FETCH_ITEMS')
    });

    switch(this.objClass){
      case Shared.classSemesters:   // Semesters
      case Shared.classTontines :{  // Tontines
        loader.present().then(()=>{
          this.store.getListIDs(this.objClass).then(res => {
            this.items = res;
          }).catch(err =>{
            this.items = []
            this.showAlert('ERROR', 'FAIL_LOAD')
          }).then(() => {loader.dismiss();});
        });
        break;
      }

      case Shared.classParticipants:{   // Participants
        loader.present().then(()=>{
          const curSemester = Shared.getCurrentSemester()
          this.store.getParticipants(curSemester).then(res => {
            let myItems = res
            this.items = res;
            myItems.num_sessions = 0
            this.store.getItem(Shared.classSemesters, curSemester).then(snap => {
              myItems.num_sessions = snap.num_sessions
              this.items = myItems;
            })
          }).catch(err =>{
            this.items = []
            this.showAlert('ERROR', 'FAIL_LOAD')
          }).then(() => {loader.dismiss();});
        });
        break;
      }

      case Shared.classCharges:{    // Charges
        loader.present().then(()=>{
          let arg = {
            full:false,
            allMembers: true,
            member: []
          }
          this.store.getCharges(arg).then(res => {
            this.items = res;
          }).catch(err =>{
            this.items = [];
          }).then(() => {loader.dismiss();});
        });
        break;
      }

      case Shared.classSessions: {    // Sessions
        loader.present().then(()=>{
          this.store.getSessions({semester:Shared.getCurrentSemester()}).then(res => {
            this.items = res;
          }).catch(err => {
            this.items = [];
            this.showAlert('ERROR', 'FAIL_LOAD')
          }).then(() => {loader.dismiss();});
        });        
        break;
      }
    }
  }

  /** Long press, view item's details
   * ================================ */
  viewDetails(item){
    let paramModal = {data:item, objClass:this.objClass};
    let modalForm  = this.modalCtrl.create('ViewerPage', paramModal);
    modalForm.present();
    modalForm.onWillDismiss((res)=>{
      // if(undefined!==res && res.status==true){
      //   this.loadData();
      // }
    });
  }

  getMissingImage(event, item){
    let newSrc = item.gender.toUpperCase().localeCompare('M')==0 ?
    Shared.malePicURL : Shared.femalePicURL;
    event.target.src = newSrc;
  }

  showAlert(title:string, msg:string) {
    let alert = this.alertCtrl.create({
      title: this.translate.instant(title),
      message: this.translate.instant(msg),
      buttons: [
        {
          text: 'OK',
          role: 'cancel',
        },
      ]
    });
    alert.present()
  }

  presentToaster(msg:string, ms_duration:number){
    const toast = this.toastCtrl.create({
      message:this.translate.instant(msg),
      duration: ms_duration,
      showCloseButton: true
    });
    toast.present();
  }

}
