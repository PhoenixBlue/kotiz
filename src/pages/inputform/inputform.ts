import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, AlertController, ToastController, LoadingController } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';

import { Shared, MAX_IMAGE_SIZE, Semester, Charge } from '../../shared/shared';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Camera } from '@ionic-native/camera';
import firebase from 'firebase';
import { FireFuncAPI } from '../../providers/firefunc/firefunc';
import { StoreAPI } from '../../providers/store/store';
import { Participant, SessionKotiz } from '../../shared/shared';


@IonicPage()
@Component({
  selector: 'page-inputform',
  templateUrl: 'inputform.html',
})
export class InputFormPage {
  @ViewChild('fileInput') fileInput;
  item:         any;
  allMembers:   any;
  allTontines:  any;
  allSemesters: any;
  objClass:     string;
  bDate:        string;
  children:     string;
  formGroup: FormGroup;
  picURL:       string;
  picURL2:      string;
  btnSaveOn:    boolean = true;
  fsFunc: any = firebase.functions();

  /** constructor
   * =============  */
  constructor(public navCtrl: NavController, public loadCtrl:LoadingController,
              private view: ViewController,
              public navParams: NavParams,   public frmBuild: FormBuilder,
              private camera: Camera,        private alertCtrl: AlertController,
              public fbFunc:FireFuncAPI,     public toastCtrl:ToastController,
              public store:StoreAPI,         public translate:TranslateService   ) {
                

    // Get parameters sent from invoking page
    this.objClass = this.navParams.get('objClass');
    this.item     = this.navParams.get('data');

    switch(this.objClass){
      case Shared.classParticipants:{
        var allPromises:any[] = [];
        allPromises.push(this.store.getListIDs(Shared.classSemesters));
        allPromises.push(this.store.getListIDs(Shared.classTontines));
        allPromises.push(this.store.getMembers({active:true}));
        Promise.all(allPromises).then(val => {
          this.allSemesters = val[0];
          this.allTontines  = val[1];
          this.allMembers   = val[2];
        }).catch(err => {
          this.showAlert('ERROR','FAIL_INIT')
        })
        if(this.item===undefined) this.item = new Participant();

        this.formGroup = frmBuild.group({
          member     : [this.item.member,   Validators.compose([Validators.required, Validators.minLength(4)])],
          semester   : [this.item.semester, Validators.compose([Validators.required, Validators.minLength(4)])],
          amount     : [this.item.amount,   Validators.compose([Validators.required])],
        });
        break;
      }

      case Shared.classSessions:
      case Shared.classSemesters: {
        this.formGroup = frmBuild.group({
          anyDate : ['', Validators.compose([Validators.required])],
        });
        break;
      }

      case Shared.classTontines:{
        this.formGroup = frmBuild.group({
          amount : ['', Validators.compose([Validators.required, Validators.pattern("^[0-9]+$")])],
          tax    : ['', Validators.compose([Validators.required, Validators.pattern("^[0-9]+$")])],
          notes  : ['', []],
        });
        break;
      }

      case Shared.classCharges:{
        this.store.getMembers({active:true}).then(res => {
          this.allMembers = res;
        })
        this.item = new Charge();
        this.item.singleAmount = false;
        this.item.allMembers   = true;
        this.formGroup = frmBuild.group({
          amounttype  : ['', Validators.compose([Validators.required])],
          amount      : ['', Validators.compose([Validators.required, Validators.pattern("^[0-9]+$")])],
          membertype  : ['', Validators.compose([])],
          members     : ['', Validators.compose([ ])],
          designation : ['', Validators.compose([Validators.required, Validators.minLength(5) ])],
          comment     : ['', Validators.compose([ ])],
        });
        const formMemberTypeControl = this.formGroup.get('membertype');
        const formMemberControl     = this.formGroup.get('members');
        this.formGroup.get('amounttype').valueChanges.subscribe(amtType => {
          if(amtType.localeCompare('0')==0){ // All members
            this.item.allMembers   = true;
            this.item.singleAmount = false;
            formMemberControl.setValidators(null);
            formMemberTypeControl.setValidators(null);
          } else {
            this.item.singleAmount = true;
            formMemberTypeControl.setValidators([Validators.required]);
          }
        });
        formMemberTypeControl.valueChanges.subscribe(memtype => {
          if(this.item!==undefined && this.item.singleAmount===true &&
             memtype.localeCompare('0')==0){ // All members
            this.item.allMembers = true;
            formMemberControl.setValidators(null);
          } else {
            this.item.allMembers = false;
            formMemberControl.setValidators([Validators.required]);
          }
        });
        break;
      }
    }
  }

  ionViewDidLoad() {  }

  /** Close modal form and go back to invoking page 
   * ================================================ */
  goBack(updated:boolean){
    this.view.dismiss({
      data:this.item,
      status:updated
    });
  }

  /** Must add item to firestore and storage if needed
   * ================================================== */
  addItem(){
    switch(this.objClass){
      // ADD MEMBER
      //------------
      case Shared.classMembers:{
        break;
      }
      // ADD PARTICIPANTS
      //------------------
      case Shared.classParticipants:{
        this.item          = new Participant();
        this.item.member   = this.formGroup.value.member;
        this.item.amount   = this.formGroup.value.amount;
        this.item.semester = this.formGroup.value.semester;
        this.item.create   = true;
        (<Participant>this.item).generateID();
        this.store.updateParticipant(this.item).then(res => {
          this.presentToaster('SAVE_SUCCESS', 1500)
          this.goBack(true);
        }).catch(err => {
          this.showAlert('ERROR','FAIL_UPDATE')
        })
        break;
      }

      // ADD SEMESTER
      //--------------     
      case Shared.classSemesters:{
        var semItem: any = new Semester(this.formGroup.value.anyDate);
        semItem.table    = this.objClass;
        semItem.create   = true;
        // Get the StoreAPI to save it into cloud
        this.btnSaveOn = false;
        this.store.insertSimpleObject(semItem).then(res => {
          this.presentToaster('SAVE_SUCCESS', 1500)
          this.goBack(true);
        }).catch(err => {
          this.showAlert('ERROR','FAIL_UPDATE')
          this.btnSaveOn = true;
        })
        break;
      }

      // ADD TONTINES
      //--------------
      case Shared.classTontines:{
        var tontItem: any = this.formGroup.value;
        tontItem.table    = this.objClass;
        tontItem.uid      = tontItem.amount.toString();
        tontItem.create   = true;
        tontItem.active   = true;
        // Get the StoreAPI to save it into cloud
        this.btnSaveOn = false;
        this.store.insertSimpleObject(tontItem).then(res => {
          this.presentToaster('SAVE_SUCCESS', 1500)
          this.goBack(true);
        }).catch(err => {
          this.showAlert('ERROR','FAIL_UPDATE')
          this.btnSaveOn = true;
        })
        break;
      }

       // ADD CHARGES
       //--------------
      case Shared.classCharges:{
        this.item.amount      = this.formGroup.value.amount;
        this.item.date        = Shared.getCurrentDate();
        this.item.comment     = this.formGroup.value.comment;
        this.item.designation = this.formGroup.value.designation;
        this.item.members     = this.formGroup.value.members
        this.store.updateCharge(this.item).then(res => {
          this.goBack(true);
        }).catch(err => {
          this.showAlert('ERROR','FAIL_UPDATE')
        })
        break;
      }

       // ADD SESSION
       //--------------
       case Shared.classSessions:{
         this.item      = new SessionKotiz(this.formGroup.value.anyDate);
         const paramArg = {semester: this.item.details.semester.uid,
                           date:     this.item.details.date}
         this.store.getSessions(paramArg).then(res =>{
           if(res===undefined || res.length < 1){
             this.goBack(true)
           } else {
             this.goBack(false)
           }
         }).catch(err => {
          this.showAlert('WARNING', 'ITEM_EXISTS')
         })
         // Check if session already exists and dismiss page with
         // value of true (not exist, can create) or false (already exists)
         // List page will open SessionPage if true.
         break;
       }      
    }
  }

  /** Get picture from camera or from file system
   * ============================================= */
  getPicture() {
    if (Camera['installed']()) {
      this.camera.getPicture({
        sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
        destinationType: this.camera.DestinationType.DATA_URL,
        targetWidth: MAX_IMAGE_SIZE,
        targetHeight: MAX_IMAGE_SIZE
      }).then((data) => {
        this.picURL = 'data:image/jpg;base64,' + data;
        console.log('from my camera plugin');
      }, (err) => {
        alert('Unable to take photo');
      })
    } else {
      console.log('no camera');
      this.fileInput.nativeElement.click();
    }
  }
  
  /** Process captured image
   * ======================= */
  processWebImage(event) {
    let reader = new FileReader();
    reader.onload = (readerEvent) => {
      
      let imageData = (readerEvent.target as any).result;
      this.picURL   = imageData ;

      var img    = new Image();

      img.onload = () => {
        // console.log('dimensions: ', img.src.length, img );
        var i = new Image();
        i.src = this.imageResize(img, MAX_IMAGE_SIZE);
        this.picURL2 = i.src;
        var metaImage = this.picURL2.split(',')[0];
        if(metaImage.split('/')[0].localeCompare('data:image')!=0){
          console.log('NOT AN IMAGE!!');
          this.picURL2 = '';
        } 
      };
      img.src = this.picURL;
    };
    reader.readAsDataURL(event.target.files[0]);
  }

  /** Resize captured image
   *  ===================== */
  imageResize(img:any, nSize1:number, nSize2?:number):any{
    var canvas = document.createElement('canvas'), max_size = nSize1, width = img.width, height=img.height;
    // If size1 and size2 defined, use them as new height and width
    if(nSize2!=undefined){
      width  = nSize2;
      height = nSize1;
    } else { // If only size1 defined, then use it as new maximum size, while keeping ratio
      if (width > height) {
        if (width > max_size) {
          height *= max_size / width;
          width = max_size;
        }
      } else {
        if (height > max_size) {
          width *= max_size / height;
          height = max_size;
        }
      }
    }
    canvas.width  = width;
    canvas.height = height;
    canvas.getContext('2d').drawImage(img, 0, 0, width, height);
    var dataUrl = canvas.toDataURL('image/jpeg');
    return dataUrl;
  }

  showAlert(title:string, msg:string) {
    let alert = this.alertCtrl.create({
      title: this.translate.instant(title),
      message: this.translate.instant(msg),
      buttons: [
        {
          text: 'OK',
          role: 'cancel',
        },
      ]
    });
    alert.present()
  }

  presentToaster(msg:string, ms_duration:number){
    const toast = this.toastCtrl.create({
      message: this.translate.instant(msg),
      duration: ms_duration,
      showCloseButton: true
    });
    toast.present();
  }
}

