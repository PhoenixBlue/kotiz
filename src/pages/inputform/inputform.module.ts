import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InputFormPage } from './inputform';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    InputFormPage,
  ],
  imports: [
    IonicPageModule.forChild(InputFormPage),
    TranslateModule.forChild({}),
  ],
})
export class InputFormPageModule {}
