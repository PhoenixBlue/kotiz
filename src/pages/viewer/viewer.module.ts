import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ViewerPage } from './viewer';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    ViewerPage,
  ],
  imports: [
    IonicPageModule.forChild(ViewerPage),
    TranslateModule.forChild({}),
  ],
})
export class ViewerPageModule {}
