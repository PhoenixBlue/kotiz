import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, AlertController, ToastController } from 'ionic-angular';
import { Shared, iCharge } from '../../shared/shared';
import { StoreAPI } from '../../providers/store/store';
import { TranslateService } from '@ngx-translate/core';

@IonicPage()
@Component({
  selector: 'page-viewer',
  templateUrl: 'viewer.html',
})
export class ViewerPage {
  objClass:string;
  item:any = {};
  data:any;
  viewCheck:boolean = false;
  
  constructor(public navCtrl: NavController, public navParams: NavParams,
              private view: ViewController,  private alertCtrl: AlertController,
              public store:StoreAPI,         public toastCtrl: ToastController,
              public translate: TranslateService) {
    this.objClass  = this.navParams.get('objClass');
    this.data      = this.navParams.get('data');
    this.viewCheck = this.navParams.get('viewCheck');
    if(this.viewCheck!==true) this.viewCheck = false;

    switch(this.objClass){
      case Shared.classMembers:{
        // "this.data" is the object, as from header details of member 
        this.item = this.data;
        this.store.getMemberDetails(this.item.uid).then(res =>{
          this.item.full = res;
        }).catch(err => {
          this.showAlert('ERROR', 'FAIL_LOAD')
        })
        break;
      }
      case Shared.classSemesters:{ // data is uid of semester
        this.store.getItem(this.objClass, this.data).then(res => {
          this.item           = res;
          this.item.uid       = this.data;
          this.item.balance   = this.item.total_in - this.item.total_out;
          this.item.year      = this.data.split('S')[0];
          this.item.semester  = this.data.split('S')[1];
        }).catch(err => {
          this.showAlert('ERROR', 'FAIL_LOAD')
        })
        break;
      }
      case Shared.classTontines:{ // "this.data" is uid of tontine
        this.store.getItem(this.objClass, this.data).then(res => {
          this.item = res;
          this.item.uid = this.data;
        }).catch(err => {
          this.showAlert('ERROR', 'FAIL_LOAD')
        })
        break;
      }
      case Shared.classCharges:{ // This is to be reviewed!!
        this.item = this.data;
        if(this.viewCheck === false || this.data.charges.length==0){
          let arg = {
            full:true,
            allMembers: false,
            member: [this.item.uid]
          }
          this.store.getCharges(arg).then(res => {
            this.item.charges = [];
            res.forEach(element => {
              var elt:iCharge = {
                amount      : element.amount,
                balance     : element.balance,
                dateCharge  : element.date,
                designation : element.designation,
                paid        : (element.paid===undefined)?0:element.paid,
                remainder   : element.balance,
                uid         : element.uid
              };
              this.item.charges.push(elt);
            });
            if(this.viewCheck === true){
              this.item.charges.forEach(element => {
                if(element.remainder === undefined){
                  element.remainder = element.balance;
                }
              });
            }
          }).catch(err => {
            this.item.charges = [];
            this.showAlert('ERROR', 'FAIL_LOAD')
          })
        }
        break;
      }
      case Shared.classParticipants:{
        // Get all charges of member, has the participant collected yet, missed contributions
        // for the charges....? if already loaded, just display. if not, load from DB, then 
        // return it back to caller.
        break;
      }
    }
  }

  ionViewDidLoad() {  }

  goBack(){
    this.view.dismiss(this.item);
  }

  getMissingImage(event, item){
    let newSrc = item.gender.toUpperCase().localeCompare('M')==0 ?
    Shared.malePicURL : Shared.femalePicURL;
    event.target.src = newSrc;
  }


  payCharge(idx:number){
    // From budget, subtract amount of the charge (remainder)
    if(this.item.budget <= 0){
      return
    }
    const initBudget = this.item.budget;
    this.item.modified = true;
    if(this.item.budget > this.item.charges[idx].remainder){
      this.item.budget -= this.item.charges[idx].remainder;
      this.item.charges[idx].remainder = 0;
    } else {
      this.item.charges[idx].remainder -= this.item.budget;
      this.item.budget = 0;
    }
    if(this.item.paid === undefined){
      this.item.paid = initBudget - this.item.budget;
    } else {
      this.item.paid += initBudget - this.item.budget;
    }
  }


  resetCharge(idx:number){
    // Reset value of remainder to match balance and update budget
    const diffPaid = this.item.charges[idx].balance - this.item.charges[idx].remainder;
    this.item.budget += diffPaid;
    this.item.charges[idx].remainder = this.item.charges[idx].balance;
    this.item.paid -= diffPaid;
  }

  collectParticipant(){
    this.item.modified = true;
    if(this.item.paid === undefined){
      this.item.paid = 0;
    }
    this.goBack()
  }


  showAlert(title:string, msg:string) {
    let alert = this.alertCtrl.create({
      title: this.translate.instant(title),
      message: this.translate.instant(msg),
      buttons: [
        {
          text: 'OK',
          role: 'cancel',
        },
      ]
    });
    alert.present()
  }


  presentToaster(msg:string, duration:number){
    const toast = this.toastCtrl.create({
      message: this.translate.instant(msg),
      duration: duration,
      showCloseButton: true
    });
    toast.present();
  }

}
