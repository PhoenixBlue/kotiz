import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, LoadingController, ItemSliding } from 'ionic-angular';
import { FireAuthAPI } from '../../providers/fireauth/fireauth';
import { FireFuncAPI } from '../../providers/firefunc/firefunc';
import { Shared, Member } from '../../shared/shared';
import firebase from 'firebase';
import { StoreAPI } from '../../providers/store/store';
import { TranslateService } from '@ngx-translate/core';

@IonicPage()
@Component({
  selector: 'page-members',
  templateUrl: 'members.html',
})
export class MembersPage {
  members:any[] = [];
  data:any;
  fsFunc: any = firebase.functions();
  filter: any = {
    filter: false,
    active: false,
    gender: 'a'
  }
  queryOptions: any = {
    range:{
      limit: 1000,
      // startValue: 'name',
      // endValue:'zzzzzzzzzzzzz'
    },
    filter:{
     // active: false,
     // gender: undefined
    }
  }

  constructor(public navCtrl: NavController, public navParams: NavParams,
              private modalCtrl: ModalController, public fbauth:FireAuthAPI,
              public loadCtrl: LoadingController, public fbFunc:FireFuncAPI,
              public store:StoreAPI, public translate: TranslateService) {
    this.filter = {}
    this.queryOptions.filter.active = true;
    this.loadData();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MembersPage');
  }

  loadData(){
    let loader = this.loadCtrl.create({
      content: this.translate.instant('FETCH_ITEMS')
    });

    loader.present().then(()=>{
      this.store.getMembers(this.queryOptions).then((res) =>{
        this.members = res;
      }).catch(err =>{
        console.log(err);
      }).then(() => {loader.dismiss();});
    });
  }

  openForm(){
    let paramModal = {data:this.data, objClass:Shared.classMembers};
    let modalForm  = this.modalCtrl.create('MemberFormPage', paramModal);
    modalForm.present();
    modalForm.onWillDismiss((res)=>{
      if(undefined!==res && res.status==true){
        this.loadData();
      }
    });
  }

  addItem(){
    // this.data = new Member();
    this.openForm();
  }

  // editData(item: any, slidingItem?:ItemSliding){
  //   if(undefined!==slidingItem){
  //     slidingItem.close();
  //   }
  //   this.data = item;
  //   this.openForm();
  // }

  // deleteData(item: ObjBkpr, slidingItem?:ItemSliding){
  //   if(undefined!==slidingItem){
  //     slidingItem.close();
  //   }
  //   this.dbHelper.delete(DbApi.tableTransact, item).then(res =>{
  //     this.showAlert('Transaction Deleted ');
  //     this.loadData();
  //   }).catch(e=>{
  //     alert(e.message);
  //   });
  // }

  // showAlert(msg:string){
  //   const alertC = this.alertCtrl.create({
  //     title: 'Transaction',
  //     subTitle: msg,
  //     buttons: ['OK']
  //   });
  //   alertC.present();
  // }

  getMissingImage(event, item){
    let newSrc = item.gender.toUpperCase().localeCompare('M')==0 ?
    Shared.malePicURL : Shared.femalePicURL;
    event.target.src = newSrc;
  }

  longPressMember(item){
    let paramModal = {data:item, objClass:Shared.classMembers};
    let modalForm  = this.modalCtrl.create('ViewerPage', paramModal);
    modalForm.present();
    modalForm.onWillDismiss((res)=>{
      // if(undefined!==res && res.status==true){
      //   this.loadData();
      // }
    });
  }

  filterMember(val: number){
    if(val==0){
      this.filter.filter = !this.filter.filter;
      return;
    }
    if(this.filter.gender=="a" && this.filter.active=="1"){
      this.filter.filter = false;
    }
    const checkFilter  = (this.filter.filter===true);

    // this.queryOptions.filter.gender = this.filter.gender;
    const filterActive = checkFilter ? this.filter.active : undefined;
    const filterGender = checkFilter ? this.filter.gender : undefined;
    let   reload:boolean = false;

    if(filterActive != this.queryOptions.filter.active){
      switch(this.filter.active){
        case '0':{
          this.queryOptions.filter.active = true;
          break;
        }
        case '1':{
          this.queryOptions.filter.active = false;
          break;
        }
        default:{
          this.queryOptions.filter.active = undefined;
          break;
        }
      }
      reload = true;
    }
    if(filterGender != this.queryOptions.filter.gender){
      this.queryOptions.filter.gender = filterGender;
      reload = true;
    }
    if(reload){
      this.loadData()
    }
  }
}
