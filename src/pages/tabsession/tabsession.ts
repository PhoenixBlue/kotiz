import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, AlertController, LoadingController, ToastController } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import { StoreAPI } from '../../providers/store/store';
import { Shared, SessionKotiz, iTotal } from '../../shared/shared';

@IonicPage()
@Component({
  selector: 'page-tabsession',
  templateUrl: 'tabsession.html',
})
export class TabSessionPage {
  title     : any;
  items     : SessionKotiz = new SessionKotiz();
  xtraTotal : iTotal = {
    designation: 'extra',
    contributed: 0,
    paid_out   : 0,
    recovered  : 0
  };
  readOnlySession:boolean = true;

  constructor(public navCtrl: NavController,  public loadCtrl: LoadingController, 
              public navParams: NavParams,    private alertCtrl: AlertController,
              public events:Events,           public toastCtrl:ToastController,
              public store: StoreAPI,         public translate:TranslateService  ) {

    this.items = this.store.getCurrentSession();
    this.events.subscribe(Shared.eventUpdateSession, (val) => {
      this.items = val;
      this.readOnlySession = (this.items.uid!==undefined);
    })
  }

  ionViewDidLoad() {
    this.items = this.store.getCurrentSession()
    this.readOnlySession = (this.items.uid!==undefined);
  }

  ionViewWillEnter(){
    let allIn = 0;
    let allOut = 0;
    this.items.allTotals.forEach(oneTotal => {
      allIn  += Number(oneTotal.contributed) + Number(oneTotal.recovered);
      allOut += Number(oneTotal.paid_out);
    })
    allIn  += Number(this.xtraTotal.contributed) + Number(this.xtraTotal.recovered);
    allOut += Number(this.xtraTotal.paid_out);
    this.items.details.all_In  = allIn;
    this.items.details.all_out = allOut;
  }

  /** EXIT SESSION PROCESSING
   * ========================
   * If 'uid' is undefined, session has not been saved.
   * Display a popup to ask confirmation of the exit
   * 
   * ---------------------------------------------------  */
  endSession(){
    // If not undefined, the session was updated in DB
    if(this.items.uid!==undefined){
      this.events.publish('sessions:close', true)
      this.store.closeCurrentSession().then(() => {}).catch(err => {console.log(err)})
      return;
    }
    // Else...
    let alert = this.alertCtrl.create({
      title: this.translate.instant('SAVE'),
      message: this.translate.instant('CONFIRM_SAVE'),
      buttons: [
        {
          text: this.translate.instant('CANCEL'),
          role: 'cancel',
          handler: () => {  }
        },
        {
          text: this.translate.instant('EXIT'),
          handler: () => {
            this.events.publish('sessions:close', true)
            this.store.closeCurrentSession().then(() => {}).catch(err => {console.log(err)})
          }
        }
      ]
    });
    alert.present();
  }

  /** SAVE SESSION
   * =============
   * For each member, update bank if amount>0, update cotisations if amount>0
   * Finally update session itself, with semester object and session object
   * 
   * -------------------------------------------------------------------------- */
  saveSession(){
    let alert = this.alertCtrl.create({
      title: this.translate.instant('SESSION'),
      message: this.translate.instant('CONFIRM_SAVE'),
      buttons: [
        {
          text: this.translate.instant('CANCEL'),
          role: 'cancel',
          handler: () => {  }
        },
        {
          text: this.translate.instant('SAVE'),
          handler: () => {
            let loader = this.loadCtrl.create({
              content: this.translate.instant('SAVING')
            });
            loader.present().then(()=>{
              this.store.updateSession(this.items).then(res =>{
                // this.items = res.data;
                this.store.setCurrentSession(this.items)
                this.presentToaster("SAVE_SUCCESS", 2000)                
              }).catch(err => {
                this.showAlert('SAVE', 'SAVE_FAIL')
              }).then(() => {loader.dismiss();});
            });
          }
        }
      ]
    });
    alert.present();
  }

  /** RESET SESSION
   * ==============
   * Put back all contributions to '0' 
   * 
   * ---------------------------------- */
  resetSession(){
    let alert = this.alertCtrl.create({
      title:  this.translate.instant('RESET'),
      message:  this.translate.instant('CONFIRM_RESET'),
      buttons: [
        {
          text: this.translate.instant('CANCEL'),
          role: 'cancel',
          handler: () => {  }
        },
        {
          text: this.translate.instant('RESET'),
          handler: () => {
            // Reset extra
            this.xtraTotal.contributed = 0;
            this.xtraTotal.paid_out    = 0;
            this.xtraTotal.recovered   = 0;
            // Reset report
            this.items = SessionKotiz.reset(this.items);
            this.presentToaster('RESET_SUCCESS', 2000)
          }
        }
      ]
    });
    alert.present();
  }


  showAlert(title:string, msg:string) {
    let alert = this.alertCtrl.create({
      title: this.translate.instant(title),
      message: this.translate.instant(msg),
      buttons: [
        {
          text: 'OK',
          role: 'cancel',
        },
      ]
    });
    alert.present()
  }


  presentToaster(msg:string, duration:number){
    const toast = this.toastCtrl.create({
      message: this.translate.instant(msg),
      duration: duration,
      showCloseButton: true
    });
    toast.present();
  }

}