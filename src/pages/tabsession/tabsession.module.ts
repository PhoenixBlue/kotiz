import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TabSessionPage } from './tabsession';

@NgModule({
  declarations: [
    TabSessionPage,
  ],
  imports: [
    IonicPageModule.forChild(TabSessionPage),
  ],
})
export class TabSessionPageModule {}