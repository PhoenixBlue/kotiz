import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TabBankPage } from './tabbank';

@NgModule({
  declarations: [
    TabBankPage,
  ],
  imports: [
    IonicPageModule.forChild(TabBankPage),
  ],
})
export class TabBankPageModule {}