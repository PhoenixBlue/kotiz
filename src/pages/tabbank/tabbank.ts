import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Events, AlertController } from 'ionic-angular';
import { StoreAPI } from '../../providers/store/store';
import { Shared, SessionKotiz } from '../../shared/shared';
import { TranslateService } from '@ngx-translate/core';


@IonicPage()
@Component({
  selector: 'page-tabbank',
  templateUrl: 'tabbank.html',
})
export class TabBankPage {
  title:any;
  items:SessionKotiz = new SessionKotiz();
  readOnlySession: boolean = true;

  constructor(public navCtrl: NavController, public navParams: NavParams,
              public store: StoreAPI,        public loadCtrl: LoadingController,
              public events: Events,         private alertCtrl: AlertController,
              public translate: TranslateService) {
    this.title = this.navCtrl.parent.getSelected().tabTitle;
    this.items = this.store.getCurrentSession();
    this.events.subscribe(Shared.eventUpdateSession, val => {
      this.items = val
      this.readOnlySession = (this.items.uid!==undefined);
    });
  }

  ionViewDidLoad() {
    this.items = this.store.getCurrentSession()
    this.readOnlySession = (this.items.uid!==undefined);
  }

  getMissingImage(event, item){
    let newSrc = item.gender.toUpperCase().localeCompare('M')==0 ?
    Shared.malePicURL : Shared.femalePicURL;
    event.target.src = newSrc;
  }

  payBank(idx:number){
    let alert = this.alertCtrl.create({
      title: this.translate.instant('BANK'),
      inputs: [
        {
          name: 'amount',
          placeholder:this.translate.instant('AMOUNT'),
          type: 'number'
        }
      ],
      buttons: [
        {
          text: this.translate.instant('CANCEL'),
          role: 'cancel',
          handler: data => { }
        },
        {
          text: this.translate.instant('PAY'),
          handler: data => {
            this.updateBankTotal(idx, data.amount);
          }
        }
      ]
    });
    alert.present();
  }

  updateBankTotal(idx:number, amount:number){
    if(amount < 0) amount = 0;
    const initAmount:number = Number(this.items.members[idx].bank.paid);
    var prevTotal:number    = 0;
    prevTotal = this.items.allTotals[0].contributed;
    this.items.allTotals[0].contributed = Number(prevTotal) - Number(initAmount) + Number(amount);
    this.items.members[idx].bank.paid   = Number(amount);
    this.store.setCurrentSession(this.items);
  }
  
}