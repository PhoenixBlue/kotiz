import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, Events } from 'ionic-angular';
import { TabSessionPage, TabCotisePage, TabBankPage } from '../pages';
import { StoreAPI } from '../../providers/store/store';
import { TranslateService } from '@ngx-translate/core';


@IonicPage()
@Component({
  selector: 'page-sessions',
  templateUrl: 'sessions.html',
})
export class SessionsPage {
  public allTabs: any[]  = [];
  item: any;
  // allTitles: any[]= [];
   
  constructor(public navCtrl: NavController, public navParams: NavParams,
              public store:StoreAPI,         private view:ViewController,
              public events: Events,         public translate: TranslateService) {
    this.item         = this.navParams.get('item');
    const createItem  = this.navParams.get('create');

    this.allTabs = [ { root: TabSessionPage, title: this.translate.instant('SESSIONS'),     icon: "calendar" },
                     { root: TabCotisePage,  title: this.translate.instant('COTISATIONS'),  icon: "cash" },
                     { root: TabBankPage,    title: this.translate.instant('BANKS'),        icon: "card" } ]

    this.events.subscribe('sessions:close', (val) => {
      if(val===true) {
        this.view.dismiss()
        this.events.unsubscribe('sessions:close')
      }
    })
  }
}
