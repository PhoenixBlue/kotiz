import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SessionsPage } from './sessions';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    SessionsPage,
  ],
  imports: [
    IonicPageModule.forChild(SessionsPage),
    TranslateModule.forChild()
  ],
})
export class SessionsPageModule {}
