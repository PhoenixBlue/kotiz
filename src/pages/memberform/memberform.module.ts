import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MemberFormPage } from './memberform';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    MemberFormPage,
  ],
  imports: [
    IonicPageModule.forChild(MemberFormPage),
    TranslateModule.forChild({}),
  ],
})
export class MemberFormPageModule {}
