import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, AlertController, ToastController } from 'ionic-angular';

import { Shared, MAX_IMAGE_SIZE } from '../../shared/shared';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { myValidator } from '../../shared/customValidator';
import { Camera } from '@ionic-native/camera';
import firebase from 'firebase';
import { FireFuncAPI } from '../../providers/firefunc/firefunc';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';


@IonicPage()
@Component({
  selector: 'page-memberform',
  templateUrl: 'memberform.html',
})
export class MemberFormPage {
  @ViewChild('fileInput') fileInput;
  item:         any;
  objClass:     string;
  bDate:        string;
  children:     string;
  public  formGroup: FormGroup;
  picURL:       string;
  picURL2:      string;
  btnSaveOn:    boolean = true;
  fsFunc: any = firebase.functions();
  selectOptions:any;

  /** constructor
   * =============  */
  constructor(public navCtrl: NavController, private view: ViewController,
              public navParams: NavParams,   private alertCtrl: AlertController,
              public frmBuild: FormBuilder,  public toastCtrl: ToastController,
              private camera: Camera,        public fbFunc:FireFuncAPI,
              private translate: TranslateService) {
    // Get parameters sent from invoking page
    this.objClass = this.navParams.get('objClass');
    this.item     = this.navParams.get('data');
    Observable.combineLatest(
      this.translate.get('MALE'),
      this.translate.get('FEMALE'),
      this.translate.get('ACTIVE'),
      this.translate.get('INACTIVE'),
    ).subscribe(data => {
      this.selectOptions = {
        male    : data[0],
        female  : data[1],
        active  : data[2],
        inactive: data[3]
      }
    })
    
    switch(this.objClass){
      case Shared.classMembers:{
        var strChildren = this.item.children.join(';');
        this.formGroup = frmBuild.group({
          surname     : [this.item.surname, Validators.compose([Validators.required, Validators.maxLength(25), 
                              Validators.minLength(4),  Validators.pattern("^[a-z 'A-Z]+$")])],
          firstName   : [this.item.firstName, Validators.compose([Validators.required, Validators.maxLength(25),
                              Validators.minLength(4), Validators.pattern("^[a-z 'A-Z]+$")])],
          gender      : [this.item.gender, Validators.compose([Validators.required])],
          birthDate   : [this.item.birthDate, Validators.compose([Validators.required, myValidator.ValidBDate])],
          birthPlace  : [this.item.birthPlace, Validators.compose([Validators.required, 
                              Validators.minLength(5), Validators.maxLength(50), Validators.pattern("^[a-z ',;A-Z]+$")])],
          email       : [this.item.email, Validators.compose([Validators.required, Validators.email])],
          phone       : [this.item.phone, Validators.compose([Validators.required, Validators.minLength(5), Validators.pattern("^[+]?[0-9]+$")])],
          occupation  : [this.item.occupation, Validators.compose([Validators.required, 
                              Validators.minLength(4), Validators.maxLength(100), Validators.pattern("^[a-z 'A-Z]+$")])],
          active      : [this.item.active?1:0, Validators.compose([Validators.required])],
          father      : [this.item.father, Validators.compose([Validators.required, Validators.maxLength(50)])],
          mother      : [this.item.mother, Validators.compose([Validators.required, Validators.maxLength(50)])],
          spouse      : [this.item.spouse, Validators.compose([Validators.required, Validators.maxLength(50)])],
          children    : [strChildren, Validators.compose([Validators.minLength(4), Validators.pattern("^[a-zA-Z][a-z 'A-Z;]+$") ]) ],
          addressHome : [this.item.addressHome, Validators.compose([Validators.required])],
          addressLocal: [this.item.addressLocal, Validators.compose([Validators.required])],
          contactHome : [this.item.contactHome, Validators.compose([Validators.required])],
          contactLocal: [this.item.contactLocal, Validators.compose([Validators.required])],
          comment     : [this.item.comment, []],
          regDate     : [this.item.regDate, Validators.compose([Validators.required, myValidator.PastDate])],
          // picture     : [this.item.picture, Validators.compose([myValidator.Photo])]
        });
        break;
      }
    }
  }

  ionViewDidLoad() {  }

  /** Close modal form and go back to invoking page 
   * ================================================ */
  goBack(){
    this.view.dismiss(this.item);
  }

  /** Must add item to firestore and storage if needed
   * ================================================== */
  addItem(){
    switch(this.objClass){
       // ADD MEMBER
       //------------
      case Shared.classMembers:{
        // Get/create references to firestore and storage
        var fbRef:firebase.firestore.DocumentReference;
        if(this.item.uid!==undefined){
          fbRef = firebase.firestore().collection('members_full').doc(this.item.uid);
          this.item.create = false;
        } else {
          this.item.create = true;
          fbRef = firebase.firestore().collection('members_full').doc();
        }
        const stRef = firebase.storage().ref('profiles/'+fbRef.id+'.jpeg');

         // If picture present... upload it then save members details
        if(this.picURL2!==undefined && this.picURL2.length > 100){
          return stRef
          .putString(this.picURL2.split(',')[1], 'base64', { contentType: 'image/jpeg' })
          .then(() => {
            return stRef.getDownloadURL().then(downloadURL => {
              this.item.picURL = downloadURL;
              this.item.uid = fbRef.id;
            }).catch(err =>{
              console.log('could not get download URL', err);
            });
          }).catch(err => {
            this.presentToaster('FAIL_LOAD', 1500)
          }).then(() => { // Member 's details must be saved even if picture upload fails
            this.addMember();
          });
        } else {  // If no picture, save member's details to firestore
          this.addMember();
        }
        break;
      }
    }
  }

  /** Get picture from camera or from file system
   * ============================================= */
  getPicture() {
    if (Camera['installed']()) {
      this.camera.getPicture({
        sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
        destinationType: this.camera.DestinationType.DATA_URL,
        targetWidth: MAX_IMAGE_SIZE,
        targetHeight: MAX_IMAGE_SIZE
      }).then((data) => {
        this.picURL = 'data:image/jpg;base64,' + data;
        console.log('from my camera plugin');
      }, (err) => {
        alert('Unable to take photo');
      })
    } else {
      console.log('no camera');
      this.fileInput.nativeElement.click();
    }
  }
  
  /** Process captured image
   * ======================= */
  processWebImage(event) {
    let reader = new FileReader();
    reader.onload = (readerEvent) => {
      
      let imageData = (readerEvent.target as any).result;
      this.picURL   = imageData ;

      var img    = new Image();

      img.onload = () => {
        var i = new Image();
        i.src = this.imageResize(img, MAX_IMAGE_SIZE);
        this.picURL2 = i.src;
        var metaImage = this.picURL2.split(',')[0];
        if(metaImage.split('/')[0].localeCompare('data:image')!=0){
          this.presentToaster('FAIL_LOAD', 1500)
          this.picURL2 = '';
        } 
      };
      img.src = this.picURL;
    };
    reader.readAsDataURL(event.target.files[0]);
  }

  /** Resize captured image
   *  ===================== */
  imageResize(img:any, nSize1:number, nSize2?:number):any{
    var canvas = document.createElement('canvas'), max_size = nSize1, width = img.width, height=img.height;
    // If size1 and size2 defined, use them as new height and width
    if(nSize2!=undefined){
      width  = nSize2;
      height = nSize1;
    } else { // If only size1 defined, then use it as new maximum size, while keeping ratio
      if (width > height) {
        if (width > max_size) {
          height *= max_size / width;
          width = max_size;
        }
      } else {
        if (height > max_size) {
          width *= max_size / height;
          height = max_size;
        }
      }
    }
    canvas.width  = width;
    canvas.height = height;
    canvas.getContext('2d').drawImage(img, 0, 0, width, height);
    var dataUrl = canvas.toDataURL('image/jpeg');
    return dataUrl;
  }
  

  /** Add member object to cloud database
   * ==================================== */
  addMember(){
    // Save some members of the object first
    var url    = (this.item.picURL!==undefined) ? this.item.picURL : '';
    var uid    = this.item.uid;
    var create = this.item.create;
    // Get object details from formGroup
    this.item           = this.formGroup.value;
    this.item.children  = this.item.children.split(';');
    this.item.active    = (this.formGroup.value.active==1);
    this.item.fullName  = this.item.firstName.split(' ')[0] +  ' ' + this.item.surname.split(' ')[0];
    this.item.picURL    = url;
    this.item.uid       = uid;
    this.item.create    = create;

    this.btnSaveOn = false;
    var fsAddMember = this.fsFunc.httpsCallable('updateMember');
    fsAddMember(this.item).then((result) => {
      // Read result of the Cloud Function.
      var sanitizedMessage = result.data;
      this.goBack(); // If success, close modal form
    }).catch( err => { // If failure, keep form opened.
      this.btnSaveOn = true;
      console.log('error callable: ', err.message, err);
    });
  }


  showAlert(title:string, msg:string) {
    let alert = this.alertCtrl.create({
      title: this.translate.instant(title),
      message: this.translate.instant(msg),
      buttons: [
        {
          text: 'OK',
          role: 'cancel',
        },
      ]
    });
    alert.present()
  }

  presentToaster(msg:string, ms_duration:number){
    const toast = this.toastCtrl.create({
      message:this.translate.instant(msg),
      duration: ms_duration,
      showCloseButton: true
    });
    toast.present();
  }


/** Test function, to enter details of multiple users at once
 * ========================================================== */
  testAm(){
    var allThem = '[]';

    this.btnSaveOn = false;
    var addMember = this.fsFunc.httpsCallable('updateMember');
    var testMembers = JSON.parse(allThem);
    var allPromise:any[] = [];
    testMembers.forEach(element => {
      element.picURL = '';
      element.active = (element.active==1);
      element.children = element.children.split(';');
      element.fullName = element.firstName.split(' ')[0] +  ' ' + element.surname.split(' ')[0];
      allPromise.push(addMember(element));
    });

    Promise.all(allPromise).then(() => {
      console.log('All members saved..?');
      this.goBack();
    }).catch(err => {
      this.btnSaveOn = true;
      console.log('error saving all', err.message, err);
    })
  }

}

