import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import { FireAuthAPI } from '../../providers/fireauth/fireauth';
import { Shared } from '../../shared/shared';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public fbauth:FireAuthAPI) {
  }

  ionViewDidLoad() {  }

  changeMenu(){  }

  openMembersPage(){
    this.navCtrl.setRoot('MembersPage');
  }

  openPage(event){
    this.navCtrl.setRoot('ListPage', {objClass:event});
  }

}
