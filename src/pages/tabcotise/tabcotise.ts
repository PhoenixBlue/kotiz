import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Tab, Events, AlertController, ModalController, ToastController } from 'ionic-angular';
import { StoreAPI } from '../../providers/store/store';
import { Shared, SessionKotiz } from '../../shared/shared';
import { TranslateService } from '@ngx-translate/core';


@IonicPage()
@Component({
  selector: 'page-tabcotise',
  templateUrl: 'tabcotise.html',
})
export class TabCotisePage {
  tontine:any;
  title:any;
  items:SessionKotiz = new SessionKotiz();
  readOnlySession: boolean = true;
  nbSessions: number;
  
  constructor(public navParams: NavParams,  private modalCtrl: ModalController,
              public store:StoreAPI,        private alertCtrl: AlertController,
              public navCtrl: NavController,public toastCtrl:ToastController,
              public events: Events,        public translate: TranslateService    ) {
    this.tontine = (<Tab>this.navCtrl).tabTitle ;
    this.title   = (<Tab>this.navCtrl).tabTitle;
    this.items   = this.store.getCurrentSession();
    this.events.subscribe(Shared.eventUpdateSession, val => {
      this.items = val;
      this.readOnlySession = (this.items.uid!==undefined);
    });

  }

  ionViewDidLoad() {
    this.items = this.store.getCurrentSession()
    this.readOnlySession = (this.items.uid!==undefined);
    console.log('ionViewDidLoad TabCotisePage');
  }

  ionViewDidEnter(){
  }

  getMissingImage(event, item){
    let newSrc = item.gender.toUpperCase().localeCompare('M')==0 ?
    Shared.malePicURL : Shared.femalePicURL;
    event.target.src = newSrc;
  }

  contribute(memberIdx:number, tontIDX:number, idx:number, upd:boolean){
    let amount = 0;
    let tontUID:string = this.items.tontine[tontIDX].uid
    if(upd === true){
      amount = this.items.members[memberIdx].tontines[tontUID][idx].paid + Number(tontUID);
    }
    this.updateContributionTotal(memberIdx, tontIDX, idx, amount);
  }

  updateContributionTotal(memberIdx:number, idx_tont:number, idx:number, amount:number){
    let tontUID:string = this.items.tontine[idx_tont].uid
    const initAmount:number = this.items.members[memberIdx].tontines[tontUID][idx].paid;
    var   prevTotal:number  = 0;
    var   tontIDX           = -1;
    
    this.items.allTotals.forEach((oneTotal, idx) =>{
      if(oneTotal.designation==tontUID){
        tontIDX = idx;
        return;
      }
    })
    if(tontIDX<0){ return; }

    // Get index of tontine in array of totals
    prevTotal = this.items.allTotals[tontIDX].contributed;
    this.items.allTotals[tontIDX].contributed = prevTotal - initAmount + amount;
    this.items.members[memberIdx].tontines[tontUID][idx].paid = amount;
    this.store.setCurrentSession(this.items);
  }


  contextMenu1(param:any){
    console.log('long press 1: ', param)
  }

  contextMenu2(m_idx:number, tontIDX:number, p_idx:number){
    let tontine = this.items.tontine[tontIDX].uid;
    console.log('long press 2 on ' + m_idx + ': ', p_idx)
    let partP_collected:boolean = true;
    if(this.items.members[m_idx].tontines[tontine][p_idx].session.length <= 0){
      partP_collected = false;
    } else {
      if(this.items.members[m_idx].tontines[tontine][p_idx].session.localeCompare(this.items.details.date)==0){
        partP_collected = false;
      } else {
        partP_collected = true;
      }
    }

    let alert = this.alertCtrl.create({
      title: 'COTISATION CONTEXT MENU',
      inputs: [
        {
          label: 'Collect for this participant',
          value: '0',
          type: 'radio',
          disabled: partP_collected
        },
        {
          label: 'Cancel collection for this participant',
          value: '1',
          type: 'radio',
          disabled: partP_collected
        },
        {
          label: 'view Participant',
          value: '2',
          type: 'radio'
        },
        {
          label: 'view member',
          value: '3',
          type: 'radio'
        },

      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: data => { }
        },
        {
          text: 'OK',
          handler: data => {
            console.log( 'value on submenu: ', data);
            this.processParticipant(data, m_idx, tontIDX, p_idx);
          }
        }
      ]
    });
    alert.present();
  }

  processParticipant(operation:string, m_idx:number, tontIDX:number, p_idx:number){
    let tontine = this.items.tontine[tontIDX].uid;
    let totalIndex = 0;
    this.items.allTotals.forEach( (element, index, Arr) => {
      if(Number(element.designation)==Number(tontine)){
        totalIndex = index;
        return
      }
    });
    let normalBudget:number = 0;
    let normalTax:number    = 0;
    this.items.tontine.forEach(oneT => {
      if(Number(oneT.uid)==Number(tontine)){
        normalBudget = Number(tontine) * 13 - oneT.tax;
        normalTax    = oneT.tax;
        return
      }
    })
    // open modal to view member, participant or charges
    switch(operation){
      case '0':{    // COLLECTION FROM A PARTICIPANT
        let paramModal = {  // member details
          data      : { 
            uid     : this.items.members[m_idx].uid,
            fullName: this.items.members[m_idx].details.fullName,
            budget  : this.items.members[m_idx].tontines[tontine][p_idx].budget,
            modified: false,
            charges : this.items.members[m_idx].charges.details // array of modified charges, can be empty
           },
          objClass  : Shared.classCharges,
          viewCheck : true,
        };
        let modalForm  = this.modalCtrl.create('ViewerPage', paramModal);
        if(this.items.members[m_idx].tontines[tontine][p_idx].session.length > 2){
          this.presentToaster('WARN_COLLECT', 2000)
          break;
        }
        modalForm.present();
        modalForm.onWillDismiss((res)=>{
          // "res" must be have the same structure as 'paramModal'
          if(res.modified){
            // if(res.paid===undefined) res.paid = 0;
            this.items.allTotals[totalIndex].paid_out   += normalBudget + normalTax
            this.items.allTotals[totalIndex].recovered  += ( Number(this.items.tontine[tontIDX].tax) + Number( res.paid))
            this.items.members[m_idx].tontines[tontine][p_idx].budget  = res.budget;
            this.items.members[m_idx].charges.details                  = res.charges;
            this.items.members[m_idx].tontines[tontine][p_idx].session = this.items.details.date;
            this.items.members[m_idx].tontines[tontine][p_idx].recvd   = res.budget;
            this.store.setCurrentSession(this.items);
          }
        });
        break;
      }

      case '1':{    // CANCEL COLLECTION
        // if budget has been modified, cancel charges where remainder!= balance
        let initBudget = this.items.members[m_idx].tontines[tontine][p_idx].budget;
        
        if(this.items.members[m_idx].tontines[tontine][p_idx].session.localeCompare(this.items.details.date)==0){
          let totalPayback = 0;
          this.items.members[m_idx].charges.details.forEach(oneCharge => {
            let toRecover = normalBudget - this.items.members[m_idx].tontines[tontine][p_idx].budget;
            if(toRecover <= 0){
              this.items.members[m_idx].tontines[tontine][p_idx].budget = normalBudget;
              return;
            }
            let toPayBack = oneCharge.balance - oneCharge.remainder;
            if(toRecover >= toPayBack){
              totalPayback += toPayBack;
              this.items.members[m_idx].tontines[tontine][p_idx].budget += toPayBack;
              oneCharge.remainder = oneCharge.balance
            } else {
              totalPayback        += toRecover;
              oneCharge.remainder += toRecover;
              this.items.members[m_idx].tontines[tontine][p_idx].budget = normalBudget;
            }
          });
          this.items.allTotals[totalIndex].paid_out  -= (normalBudget + normalTax);
          this.items.allTotals[totalIndex].recovered -= (normalTax + totalPayback)
          this.items.members[m_idx].tontines[tontine][p_idx].session = "";
          this.presentToaster('CANCEL_COLLECT', 2000)
          this.store.setCurrentSession(this.items);
        } else {
          this.presentToaster('NOT_COLLECTED', 2000)
        }
        break;
      }
      case '2': console.log('view participant: ', this.items.members[m_idx].tontines[tontine][p_idx]); break;
      case '3': console.log('view member: ', this.items.members[m_idx].details); break;
      default: break;
    }
  }

  presentToaster(msg:string, ms_duration:number){
    const toast = this.toastCtrl.create({
      message: this.translate.instant(msg),
      duration: ms_duration,
      showCloseButton: true
    });
    toast.present();
  }

}