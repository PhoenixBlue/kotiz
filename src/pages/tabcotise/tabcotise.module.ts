import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TabCotisePage } from './tabcotise';

@NgModule({
  declarations: [
    TabCotisePage,
  ],
  imports: [
    IonicPageModule.forChild(TabCotisePage),
  ],
})
export class TabCotisePageModule {}