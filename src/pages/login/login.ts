import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ToastController } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import firebase from 'firebase';
import { FireAuthAPI } from '../../providers/fireauth/fireauth';
import { HomePage } from '../home/home';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  loginPassword:string;
  loginEmail:string;

  constructor(public navCtrl: NavController, public translate: TranslateService,
              public navParams: NavParams,   public toastCtrl: ToastController,
              public fAuth: FireAuthAPI,     private alertCtrl: AlertController) {
    firebase.auth().onAuthStateChanged(user => {
      this.presentToaster('USER_LOGGED', 1500)
    });
  }

  ionViewDidLoad() {
  }

  loginUser(){
    this.fAuth.loginUser(this.loginEmail, this.loginPassword).then(authData => {
      this.presentToaster('USER_LOGGED', 1500)
      this.navCtrl.setRoot(HomePage);
    }, error => {
      this.showAlert('ERROR', 'FAIL_LOGIN')
    });
  }

  showAlert(title:string, msg:string) {
    let alert = this.alertCtrl.create({
      title: this.translate.instant(title),
      message: this.translate.instant(msg),
      buttons: [
        {
          text: 'OK',
          role: 'cancel',
        },
      ]
    });
    alert.present()
  }

  presentToaster(msg:string, ms_duration:number){
    const toast = this.toastCtrl.create({
      message:this.translate.instant(msg),
      duration: ms_duration,
      showCloseButton: true
    });
    toast.present();
  }

}
