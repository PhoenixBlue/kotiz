import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';


@Injectable()
export class FireFuncAPI {

  constructor(public http: HttpClient) {
    console.log('Hello FireFuncAPI Provider');
  }

  getAllMembers(){
     return this.http.get('https://us-central1-armaspretoria-c392911e60fb9e3.cloudfunctions.net/getMembers');
  }

  addMember(item){
    return this.http.post('https://us-central1-armaspretoria-c392911e60fb9e3.cloudfunctions.net/addMember', JSON.stringify(item), {responseType: 'text'});
  }

  updateMember(item){
    return this.http.post('https://us-central1-armaspretoria-c392911e60fb9e3.cloudfunctions.net/updateMember', JSON.stringify(item), {responseType: 'text'});
  }

}
