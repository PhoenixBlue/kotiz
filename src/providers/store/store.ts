import firebase from 'firebase';
import { Storage } from '@ionic/storage';
import { Injectable } from '@angular/core';
import { Platform, Events } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import { Shared, SessionKotiz, iTotal, cloudUpdates, CustomDate, iCharge, iSessMember } from '../../shared/shared';

enum STATUS {
  fail = -1,
  ok,
  permission_denied
}


@Injectable()
export class StoreAPI {
  fsFunc: any = firebase.functions();
  curSession: SessionKotiz = new SessionKotiz();

  constructor(private localStore: Storage, public platform:Platform, public events:Events,
    public translate: TranslateService) {
    console.log('Hello StoreAPI Provider: platform is ', 
          this.detectMobilePlatform()?'mobile':'web');
  }

  /** GET MEMBERS FROM FIRESTORE
   * ===========================
   * Get list of members from the cloud database
   * --------------------------------------------- */
  getMembersFirestore(queryOptions:any):Promise<any>{
    let members:any[] = [];
    return new Promise((resolve, reject) => {
      var getMembers = this.fsFunc.httpsCallable('getMembers');
      getMembers(queryOptions).then((result) => {
        members = result.data.data; // Read result of the Cloud Function,
        let members2:any = [];
        members.forEach(element => {
          if(element.active===true)
            members2.push(element)
        });
        let checkCache = queryOptions.filter.active===true;
        checkCache = checkCache || (queryOptions.filter.active===undefined)
        checkCache = checkCache && (queryOptions.filter.gender===undefined || queryOptions.filter.gender.toLowerCase()=='a')
        if(checkCache===true){
          this.localStore.set('members', members2);
        }
        resolve(members); //  and return 
      }).catch( err => {  // return error
        reject(err);
      });
    });
  }

  /** GET MEMBERS FROM LOCAL STORE
   * =============================
   * If no members in local storage, fetch them from FireStore and
   * keep a copy in local storage. Filters are applied locally.
   * 
   * ---------------------------------------------------------------  */
  getMembersLocalStore(queryOptions:any):Promise<any>{
    if(queryOptions === undefined){
      queryOptions = {
        range:{
          limit: 1000,
        },
        filter:{
          active: true
        }
      }
    }

    return new Promise((resolve, reject) =>{
      this.localStore.get('members').then((res) => {
        let members:any[] = [];
        members = res;
        const nbrMembers = members.length;
        if(nbrMembers>0){
          if(queryOptions.filter.gender.toLowerCase()=="f" || 
             queryOptions.filter.gender.toLowerCase()=="m" ){
            let idx = nbrMembers-1;
            while(idx>=0){
              if(members[idx].gender.toLowerCase() != queryOptions.filter.gender.toLowerCase()){
                members.splice(idx, 1)
              }
              idx--;
            }
          }
          resolve(members);
        } else {
          this.getMembersFirestore(queryOptions).then(res => {
            resolve(res);
          }).catch(err2 => {
            reject(err2);
          })
        }
      }).catch(err => {
        this.getMembersFirestore(queryOptions).then(res => {
          resolve(res);
        }).catch(err2 => {
          reject(err2);
        })
      });
    });
  }

  /** CHECK UPDATES BEFORE GETTING MEMBERS
   * ===================================== */
  getMembers(queryOptions:any):Promise<any>{
    // first get updates from localStore
    // if latest update is less than 10 min
    
    // return this.getUpdates().then(res => {
    //   const resVal:any = res;
    //   console.log("updates ", res)
    //   let   dd  = new CustomDate()
    //   let   upd = new CustomDate()
    //   dd.setISOString("2019-08-28T14:46:21.215Z")
    //   upd.setISOString(resVal.data.members)
    //   if(dd.getISODate() <= upd.getISODate()){
    //     this.localStore.remove(Shared.classMembers.toLowerCase())
    //   }
      return this.getMembersLocalStore(queryOptions);
    // })
  }

  /** GET FULL DETAILS OF A MEMBER FROM CLOUD DB 
   * ============================================  */
  getMemberDetails(memID:string):Promise<any>{
    return new Promise((resolve, reject) => {
      var getFullMember = this.fsFunc.httpsCallable('getFullMember');
      getFullMember(memID).then((result) => {
        resolve(result.data.data);
      }).catch(err => {
        reject(err);
      });
    });
  }

  /** DETECT OPERATING PLATFORM
   * ===========================  */
  detectMobilePlatform():boolean{
    return this.platform.is('cordova');
  }

  /** CREATE A NEW SEMESTER/TONTINE
   * =============================== */
  insertSimpleObject(item:any):Promise<any>{
    return new Promise((resolve, reject) => {
      var updateItem = this.fsFunc.httpsCallable('updateSimpleCollection');
      updateItem(item).then((result) => { //Updated item in fireStore
        this.getListIDsFireStore(item.table).then(res1 => { // Update local list
          resolve('Saved and updated');
        }).catch(err1 => {
          resolve('saved but not updated locally');
        })
      }).catch(err => {
        reject(err);
      });
    })
  }

  /** Get list of IDs of a specific collection 
   * ==========================================
   * List is fetched locally, but if absent, then we
   * request the list from the firestore database
   * 
   * ------------------------------------------------ */
  getListIDs(table: string):Promise<any>{
    let allItems:any[]=[];
    return new Promise<any>((resolve, reject) => {
      this.localStore.get(table.toLowerCase()).then(res => {
        allItems = res;
        if(allItems!==null && allItems!==undefined && allItems.length > 0){
          if(table.localeCompare(Shared.classTontines)==0){
            allItems = [];
            res.forEach(element => {
              allItems.push(element.uid)
            });
          }
          resolve(allItems);
        } else {
          if(table.localeCompare(Shared.classTontines)==0){
            this.getListItemsFireStore(table).then(res => {
              if(table.localeCompare(Shared.classTontines)==0){
                allItems = [];
                res.forEach(element => {
                  allItems.push(element.uid)
                });
              }
              resolve(res);
            }).catch(err => {
              reject(err);
            })
          } else {
            this.getListIDsFireStore(table).then(res => {
              resolve(res);
            }).catch(err => {
              reject(err);
            })
          }
        }
      }).catch(err1 => {
        reject(err1);
      });
    })
  }

  /** Get list of a specific collection 
   * ==========================================
   * List is fetched locally, but if absent, then we
   * request the list from the firestore database
   * 
   * ------------------------------------------------ */
  getListItems(table: string):Promise<any>{
    let allItems:any[]=[];
    return new Promise<any>((resolve, reject) => {
      this.localStore.get(table.toLowerCase()).then(res => {
        allItems = res;
        if(allItems!==null && allItems!==undefined && allItems.length > 0){
          resolve(allItems);
        } else {
          this.getListItemsFireStore(table).then(res => {
            resolve(res);
          }).catch(err => {
            reject(err);
          })
        }
      }).catch(err1 => {
        reject(err1);
      });
    })
  }

  /** Get list of IDs from firestore database
   * ======================================== */
  getListIDsFireStore(table:string):Promise<any>{
    let allItems: any[] = [];
    return new Promise<any>((resolve, reject) => {
      var getIDs = this.fsFunc.httpsCallable('getListIDs');
      getIDs(table.toLowerCase()).then((result) => {
        allItems = JSON.parse(result.data.data);
        allItems.sort((n1, n2) => n1.localeCompare(n2)>1?-1:1);
        this.localStore.set(table.toLowerCase(), allItems);
      resolve(allItems);
      }).catch(err => {
        reject(err);
      });
    });
  }

  /** Get list of items from firestore database
   * ======================================== */
  getListItemsFireStore(table:string):Promise<any>{
    let allItems: any[] = [];
    return new Promise<any>((resolve, reject) => {
      var getIDs = this.fsFunc.httpsCallable('getListItems');
      getIDs(table.toLowerCase()).then((result) => {
        allItems = JSON.parse(result.data.data);
        allItems.sort((n1, n2) => n1.uid.localeCompare(n2.uid)>1?-1:1);
        this.localStore.set(table.toLowerCase(), allItems);
      resolve(allItems);
      }).catch(err => {
        reject(err);
      });
    });
  }

  /** Get a single item from firestore
   * ================================= */
  getItem(col:string, doc:string):Promise<any>{
    return new Promise<any>((resolve, reject) => {
      const data = {table:col, uid:doc};
      const getSingleItem = this.fsFunc.httpsCallable('getItemDetails');
      getSingleItem(data).then((result) =>{
        resolve(result.data.data);
      }).catch(err => {
        reject(err)
      })
    });
  }

  /** Create / Update a participant
   * =============================== */
  updateParticipant(partP:any):Promise<any>{
    return new Promise<any>((resolve, reject) => {
      const saveParticipant = this.fsFunc.httpsCallable('updateParticipant');
      saveParticipant(partP).then((result) => {
        resolve(result);
      }).catch(err => {
        reject(err);
      })
    });
  }

  /** Get list of participants
   * ==========================
   * Only participants of the current semester are saved in the local
   * storage. All other semesters' are accessible only from firestore.
   * 
   * ------------------------------------------------------------------ */
  getParticipants(semester?:any):Promise<any>{
    // Get current semester
    let curSem = Shared.getCurrentSemester();
    let allItems:any;
    if(semester===undefined){
      semester = curSem;
    }

    return new Promise<any>((resolve, reject) => {
      this.localStore.get(Shared.classParticipants.toLowerCase()).then(res => {
        allItems = res;
        var subPromise:Promise<any>;

        if(allItems!==null && allItems!==undefined && allItems.length > 0){
          subPromise = new Promise<any>((resolve1, rej) => { resolve1(allItems); })
        } else { // Get records from firestore
          subPromise = this.getFirestoreParticipants(semester);
        }
        subPromise.then(rawParticipants => {
          rawParticipants.members.forEach((oneMember, idx, ArrMember) => {
            if(oneMember.tontines === undefined || oneMember.tontines === null){
              ArrMember[idx].tontines = {}
              rawParticipants.tontines.forEach(oneTT => {
                ArrMember[idx].tontines[oneTT.uid] = []
              });
            }
          });
          if(curSem.localeCompare(semester)==0){
            return this.localStore.set(Shared.classParticipants.toLowerCase(), rawParticipants).then(rr =>{
              resolve(rawParticipants)
            });
          }
          resolve(rawParticipants)
        }).catch(err => {   reject (err);   })
      })
    });
  }

  /** Get list of participants from firestore DB
   * =========================================== */
  getFirestoreParticipants(semester:any):Promise<any>{
    let allItems:any = {};
    let allRequest: Promise<any>[] = [];
    var getFSParticipants = this.fsFunc.httpsCallable('getParticipants');

    allRequest.push(getFSParticipants(semester));
    allRequest.push(this.getListIDs(Shared.classTontines.toLowerCase()));
    allRequest.push(this.getMembers(undefined))

    return new Promise<any>( (resolve, reject) => {
      Promise.all(allRequest).then(responses => {
        const theParticipants = responses[0].data.data
        const theTontines     = responses[1]
        const theMembers      = responses[2]
        allItems.members = []
        theMembers.forEach(oneMember => {
          let item:any = {}
          item.tontines = theParticipants[oneMember.uid]
          item.uid      = oneMember.uid
          item.details  = {
            picURL  : oneMember.picURL,
            gender  : oneMember.gender,
            fullName: oneMember.fullName,
            active  : oneMember.active
          }
          allItems.members.push(item)
        });
        allItems.tontines = theTontines
        resolve( allItems )
      })
    })
  }

  /** Get all charges of all members
   * ================================ */
  getCharges(filter:any):Promise<any>{

    let request:Promise<any>;
    if(filter.full !==true){ // If only totals needed, first check in local store
      request = this.localStore.get(Shared.classCharges.toLowerCase());
    } else { // Get details from firestore
      request = this.getFirestoreCharges(filter);
    }

    return new Promise<any>((resolve, reject) => {
      request.then(res => {
        let allItems:any[] = [];
        let chargeTable:any[]=[];
        let requestCharges: Promise<any>;
        let requestMembers: Promise<any> = this.getMembers(undefined);

        chargeTable = res;
        // Make sure we have the charges from local or firestore
        if( chargeTable===null || (filter.full!==true && chargeTable.length <1)){
          requestCharges = this.getFirestoreCharges(filter);
        } else {
          requestCharges = new Promise<any>((resolve, reject) => {resolve(res)})
        }
        // request now members corresponding to the charges
        requestCharges.then(resultCharge => {
          if(filter.full===true)
            resolve(resultCharge);
          if (resultCharge===null || resultCharge.length==0){ resolve(resultCharge) }
          else{
            requestMembers.then(membersList => {
              resultCharge.forEach(chargeElt => {
                membersList.forEach(MemElt => {
                  if(MemElt.uid.localeCompare(chargeElt.uid)==0){
                    const item = {...chargeElt, ...MemElt};
                    allItems.push(item);
                  }
                });
              });
              if(filter.full!==true && filter.allMembers===true){
                this.localStore.set(Shared.classCharges.toLowerCase(), resultCharge);
              }
            allItems.sort((n1, n2) => n1.fullName.localeCompare(n2.fullName));
              resolve(allItems);
            })
          }
        }).catch(err1 => {
          reject(err1)
        })
      }).catch(err => {
        reject(err);
      })
    })
  }

  /** Get FireStore charges
   * ====================== */
  getFirestoreCharges(arg): Promise<any>{
    let allCharges:any[] = [];
    return new Promise<any>((resolve, reject) => {
      var getFSCharges = this.fsFunc.httpsCallable('getChargesDetails');
      getFSCharges(arg).then((result) => {
        if(result!==undefined && result.data!==null && result.data.status==0){
          allCharges = result.data.data;
          resolve(allCharges)
        } else {
          reject(result)
        }
      }).catch(err => {
        reject(err);
      });
    });
  }

  /** Insert / Update charges 
   * ======================== */
  updateCharge(chargeItem):Promise<any>{
    return new Promise<any>((resolve, reject) => {
     const chargeUpdate = this.fsFunc.httpsCallable('updateCharge');
     chargeUpdate(chargeItem).then((result) => {
       resolve(result);
     }).catch(err => {
       reject(err);
     })
   });
  }

  /** Insert / Update bank contributions 
   * ==================================== */
  updateBank(item):Promise<any>{
    return new Promise<any>((resolve, reject) => {
      const bankUpdate = this.fsFunc.httpsCallable('updateBank');
      bankUpdate(item).then((result) => {
        resolve(result);
      }).catch(err => {reject(err)});
    })
  }

  /** Get bank details
   * ==================  */
  getBankDetails(arg):Promise<any>{
    let request   : Promise<any>;
    let bankList  : any[] = [];
    let allItems  : any[] = [];

    if(arg.full!==true){ // Get requested details
      request = this.localStore.get(Shared.classBanks.toLowerCase());
    } else {
      request = this.getFirestoreBankDetails(arg);
    }

    return new Promise<any>((resolve, reject) => {
      request.then(res => {
        let bankTable   : any[] = [];
        let requestBank : Promise<any>;
        bankTable = res;

        // Make sure we ultimately get the same thing as in the firestore database
        if(bankTable===null || bankTable===undefined || (arg.full!==true && bankTable.length<1)){
          requestBank = this.getFirestoreBankDetails(arg);
        } else {
          requestBank = new Promise<any>((resolve, rej) => { resolve(bankTable); });
        }
        requestBank.then( resBank => {
          if( arg.full===true || resBank===null || resBank===undefined) { resolve(resBank)}
          else {
            bankList = resBank;
            this.getMembers(undefined).then(memberList => {
              memberList.forEach(oneMember => {
                let myBank: any = {total:0, member:''};
                bankList.forEach(oneBank => {
                  if(oneBank.member.trim().localeCompare(oneMember.uid.trim())==0){
                    myBank = oneBank;
                    return;
                  }
                })
                const item = {...oneMember, ...myBank};
                allItems.push(item);
              });
              if(arg.full!==true){
                this.localStore.set(Shared.classBanks.toLowerCase(), allItems);
              }
              allItems.sort((n1, n2) => n1.fullName.localeCompare(n2.fullName)>1?-1:1);
              resolve(allItems);
            })
          }
        })
      })
    });

  }

  /** GET BANK DETAILS FROM FIRESTORE
   * ================================ */
  getFirestoreBankDetails(arg):Promise<any>{
   return new Promise<any>((resolve, reject) => {
     var getDetails = this.fsFunc.httpsCallable('getBankDetails');
     getDetails(arg).then((result) => {
       if(result.data!==undefined){
         resolve(result.data.data)
       } else {
         reject(result)
       }
     }).catch(err => {
       reject(err);
     });
   });    
  }

  /** GET SESSIONS LIST
   * ================== */
  getSessions(data):Promise<any>{
    let request: Promise<any>;
    let curSem = '' + (new Date()).getFullYear() + 'S' + ((new Date()).getMonth()<=5?1:2);
    // If current semester, look into local storage first.
    if(curSem.localeCompare(data.semester)==0){
      request = this.localStore.get(Shared.classSessions.toLowerCase());
    } else { // If not current semester, go straight to firestore
      request = this.getFirestoreSessions(data);
    }
    
    return new Promise<any>((resolve, reject) => {
      request.then(res => {
        if(curSem.localeCompare(data.semester)==0){
          if(res===null || res===undefined || res.length <1){
            this.getFirestoreSessions(data).then(res2 =>{
              if(res2===null) 
                res2 = [];
              if(data.date===undefined)
                this.localStore.set(Shared.classSessions.toLowerCase(), res2);
              resolve(res2)
            }).catch(err2 => {reject(err2)})
          } else {
            if(data.date===undefined){
              this.localStore.set(Shared.classSessions.toLowerCase(), res);
              resolve(res)
            }
            else{
              let sessFound = undefined;
              res.forEach(element => {
                if(element.uid.localeCompare(data.date)==0){
                  sessFound = element;
                  return
                }
              });
              resolve(sessFound)
            }
          }
        } else {
          if(res===null){
            res = [];
            if(data.date!==undefined)
            res = undefined;
          } 
          resolve(res)
        }
      }).catch(err => {
        reject(err)
      })
    });
  }

  /** GET SESSIONS FROM FIRESTORE DB
   * =============================== */
  getFirestoreSessions(data):Promise<any>{
    return new Promise<any>((resolve, reject) => {
      var getSess = this.fsFunc.httpsCallable('getSessions');
      getSess(data).then((result) => {
        if(result.data!==undefined){
          if(result.data.data===undefined || result.data.data===null){
            result.data.data=[]
          }
          result.data.data.sort((n1, n2) => 
          n2.uid.localeCompare(n1.uid))
          resolve(result.data.data)
        } else {
          reject(result)
        }
      }).catch(err => {
        reject(err)
      })
    })
  }

  /** INITIALIZE A NEW SESSION
   * ==========================
   * Initialize a temporary session in local storage.
   * Generate list of participant for each cotisation and bank
   * Create a Session object too. Save all under one single object
   * in the local store, under the label "current_session"
   * 
   * --------------------------------------------------------------- */
  initSession(data) : Promise<any> {
    // Data contains the date and semester. Get corresponding year(used for bank)
    data.year           = data.semester.split('S')[0];
    let tempMembers     = [];
    let finalObject     = new SessionKotiz();
    let allRequest: Promise<any>[] = [];
    let chargeArg       = {
                            full:false,
                            allMembers: true,
                            member: ['']
                          }
    allRequest.push(this.getParticipants(data.semester));               // Get tontine participants
    allRequest.push(this.getBankDetails({full:false, year:data.year})); // Get bank participants
    allRequest.push(this.getCharges(chargeArg));                        // Get charges
    allRequest.push(this.getItem(Shared.classSemesters, data.semester));// Get semester details

    return new Promise<any>((resolve, reject) => {
      Promise.all(allRequest).then(responses => {
        // Set date of the session
        finalObject.details.date = data.date;
        // Get tontines
        finalObject.tontine = responses[0].tontines;
        // Get members details & participants
        tempMembers = responses[0].members;
        // In case some members do not have any valid tontines, create empty arrays
        tempMembers.forEach((oneMember, idx, ArrMember) => {
          if(oneMember.tontines === undefined || oneMember.tontines === null){
            ArrMember[idx].tontines = {}
            finalObject.tontine.forEach(oneTT => {
              ArrMember[idx].tontines[oneTT.uid] = []
            });
          }
        });
        // Dealing with banks details
        const allBanks = responses[1];
        allBanks.forEach(oneBank => {
          const uid = oneBank.uid;
          tempMembers.forEach((oneMem, memIDX, Arr) => {
            if(oneMem.uid.localeCompare(uid)==0){
              Arr[memIDX].bank = {total:oneBank.total, paid:0}
              return
            }
          })
        });

        // Dealing with totals:
        {
          let oneTotal:iTotal = {
            contributed: 0,
            designation: this.translate.instant('BANKS'),
            paid_out   : 0,
            recovered  : 0
          };
          finalObject.allTotals.push(oneTotal);
        }
        finalObject.tontine.forEach(oneT =>{
          let oneTotal:iTotal = {
            contributed : 0,
            designation : String(oneT.uid),
            paid_out    : 0,
            recovered   : 0
          }
          finalObject.allTotals.push(oneTotal);
        })

        // Dealing with charges
        const myCharges = responses[2];
        let   allCharges = {};
        myCharges.forEach(oneCharge => {
          allCharges[oneCharge.uid] = oneCharge.total;
        });

        // Dealing with semester details:
        const mySemester = responses[3];
        finalObject.details.semester.in  = mySemester.total_in;
        finalObject.details.semester.out = mySemester.total_out;
        finalObject.details.semester.nb_sessions = mySemester.num_sessions;

        finalObject.members = tempMembers;
        
        this.localStore.set(Shared.currentSession, finalObject);
        this.setCurrentSession(finalObject);
        resolve(finalObject); 
      }).catch(err => reject(err));
    })
  }

  /** CLOSE SESSION
   * ============== */
  closeCurrentSession():Promise<any>{
    return this.localStore.remove(Shared.currentSession)
  }

  /** SAVE A SESSION INTO DATABASE
   * ============================== 
   * First finalize the Session object, updating individual totals, bank & charges,
   * then semester details and year details where applicable(bank). Finally save to fireStore
   * 
   * ----------------------------------------------------------------------------------------- */
  updateSession(data:SessionKotiz): Promise<any>{

    // For each member, update total bank, total tontine & total charges
    let updateCharges      : boolean = false;
    let updateBank         : boolean = false;

    data.members.forEach(oneMember => {
      if(Number(oneMember.bank.paid)>0){
        updateBank = true;
      }
      oneMember.bank.total = Number(oneMember.bank.total) + Number(oneMember.bank.paid);
      oneMember.charges.details.forEach(oneCharge => {
        oneCharge.paid          =  Number(oneCharge.balance) - Number(oneCharge.remainder);
        if(oneCharge.paid>0){
          oneCharge.balance       =  Number(oneCharge.remainder);
          oneMember.charges.total -= oneCharge.paid;
          updateCharges           = true;
        }
      })
      for(var oneTontine in oneMember.tontines){
        let levelPenalty = 0;
        oneMember.tontines[oneTontine].forEach(onePartP => {
          onePartP.total += Number(onePartP.paid);
          if(levelPenalty<2){
            if(onePartP.paid < Number(oneTontine)){
              const arrears:number = (onePartP.total/Number(oneTontine)) - data.details.semester.nb_sessions;
              if(arrears < 0){ // Make sure contribution wasn't made on a previous date
                levelPenalty = 2;
                if(onePartP.session.length < 5){
                  levelPenalty = 1;
                }
              }
            }
          }
        });
        if(levelPenalty > 0){
          const failCharge:iCharge = {
            amount:       Number(oneTontine) * levelPenalty * 0.25,
            balance:      Number(oneTontine) * levelPenalty * 0.25,
            dateCharge:   data.details.date,
            designation:  "failed contribution",
            paid:         0,
            remainder:    Number(oneTontine) * levelPenalty * 0.25,
            uid:          undefined,
          }
          oneMember.charges.details.push(failCharge);
          oneMember.charges.total += failCharge.balance;
        }
      }
    });
    // Update semester & session details.
    data.details.semester.in  += Number(data.details.all_In);
    data.details.semester.out += Number(data.details.all_out);
    data.details.semester.nb_sessions++;
    // Set the uid of the object
    data.uid = data.details.date;

    return new Promise((resolve, reject) => {

      var saveSessFunc = this.fsFunc.httpsCallable('saveSession');
      saveSessFunc(data).then((result) => {
        console.log("from store saving session: ", result)
        if( result.data.status===STATUS.ok ){
          // TODO: Remove some items from local store
          // to force app to reload from cloud database
          this.localStore.remove(Shared.classSessions.toLowerCase());
          this.localStore.remove(Shared.classSemesters.toLowerCase());
          this.localStore.remove(Shared.currentSession.toLowerCase());
          this.localStore.remove(Shared.classParticipants.toLowerCase());
          if(updateBank)    this.localStore.remove(Shared.classBanks.toLowerCase());
          if(updateCharges) this.localStore.remove(Shared.classCharges.toLowerCase());
          // this.localStore.remove(Shared.classCharges.toLowerCase());
          resolve(result.data)
        } else {
          reject(result);
        }
      }).catch(err => {
        reject(err);
      })

    })
  }

  /** SET CURRENT (GLOBAL) SESSION
   * ============================= */
  setCurrentSession(val:SessionKotiz){
    this.curSession = val;
    this.events.publish(Shared.eventUpdateSession, this.curSession);
  }

  /** GET CURRENT (GLOBAL) SESSION
   * =============================  */
  getCurrentSession():SessionKotiz{
    return this.curSession;
  }

  /** TODO: WHY DID I DO THAT ???
   * ============================= */
  getUpdates(arg?:string){
    // Generate UTC date from current date
    // Get updates from local store if exist
    // Compare dates from current date and local store
    // Retrieve cloud updates if needed
    return new Promise((resolve, reject) =>{
      this.localStore.get(Shared.classUpdates.toLowerCase()).then((res) => {
        const localUpdate:cloudUpdates = res;
        let   validUpdate:boolean      = true;
        if(localUpdate !== undefined && localUpdate !== null){  // there is something in local storage
          // Check last update
          const thisDate   = new CustomDate();
          let   thisUpdate = new CustomDate();
          thisUpdate.setISOString(localUpdate.lastUpdate);
          const diffDate   = thisDate.getISODate().getTime() - thisUpdate.getISODate().getTime();
          if(diffDate > Shared.updateInterval){
            validUpdate = false;
          }
        } else {
          validUpdate = false;
        }
        if(!validUpdate){
          let func = this.getFirestoreUpdates();
          if(arg!==undefined && arg!==null){
            func = this.getFirestoreUpdates(arg);
          }
          func.then(res1 =>{
            resolve(res1)
          }).catch(err1 =>{
            reject(err1)
          })
        } else {
          if(arg!==undefined && arg!==null){
            resolve(localUpdate[arg])
          } else {
            resolve(localUpdate)
          }
        }
      }).catch(err => {
        this.getFirestoreUpdates().then(res => {
          resolve(res);
        }).catch(err2 => {
          reject(err2);
        })
      });
    });
  }

  /** GET LATEST UPDATE TIMES FROM CLOUD DB
   * =======================================  */
  getFirestoreUpdates(arg?:string){
    var getUpdatesFunc = this.fsFunc.httpsCallable('getUpdates');
    return new Promise((resolve, reject) => {
      getUpdatesFunc(arg).then((result) => {
        let resUpdate:cloudUpdates;
        const myDate = new CustomDate()
        resUpdate = {...result.data, ...{lastUpdate: myDate.getISOString()} }
        resolve(resUpdate)
      }).catch(err => {
        reject(err)
      })
    })
  }

  /** SET UPDATE TIMES ON THE CLOUD DB
   * ================================= */
  setFirestoreUpdates(data:any){
    var setUpdateFunc = this.fsFunc.httpsCallable('setUpdates');
    return new Promise((resolve, reject) =>{
      setUpdateFunc().then(result =>{
        // Updates the current one from local store
      }).catch(err => {
        reject(err)
      })
    });
  }

  /** REMOVE LOCAL UPDATES
   * =====================
   * This is to force the application to fetch updates from firestore
   * 
   * ---------------------------------------------------------------- */
  resetLocalUpdate(data?:string){
    if(data===undefined || data ===null){
      return this.localStore.remove(Shared.classUpdates.toLowerCase());
    } else {
      // Get local update, modify specific member and save it back
      return this.localStore.get(Shared.classUpdates.toLowerCase()).then(upd => {
        if(upd!==null && upd !==undefined){
          if(upd[data] === undefined || upd[data]===null){
            delete upd[data]
          }
        }
        return this.localStore.set(Shared.classUpdates.toLowerCase(), upd);
      })
    }
  }


  myUpdates(){
    let allPromises:Promise<any>[] = [];
  }


  tempCurrentSession():Promise<any>{
    return new Promise((resolve, reject) => {
      this.localStore.get(Shared.currentSession).then(res => {
        this.setCurrentSession(res);
        resolve(res)
      })
    });
  }

}
