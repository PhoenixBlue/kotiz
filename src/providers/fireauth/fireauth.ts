import { Injectable } from '@angular/core';
import firebase from 'firebase';

@Injectable()
export class FireAuthAPI {
  constructor() {  }

  /**  LOGIN A USER
   * ==============
   * Login user given email and password
   * -----------------------------------  */
  loginUser(email:string, password:string): Promise<any> {
    return firebase.auth().signInWithEmailAndPassword(email, password);
  }

  /** SIGNUP USER
   * ============
   * May be moved to cloud function
   */
  signupUser(email: string, password: string): Promise<any> {
    return firebase.auth().createUserWithEmailAndPassword(email, password).then(newUser => {
      firebase
      .database()
      .ref(`/userProfile/${newUser.user.uid}/email`)
      .set(email);
    })
    .catch(error => {
      console.error(error);
      throw new Error(error);
    });
  }

  /** RESET PASSWORD
   * ===============
   * Reset password, given email
   * ---------------------------- */
  resetPassword(email:string): Promise<void> {
    return firebase.auth().sendPasswordResetEmail(email);
  }

  /** LOGOUT USER 
   * ============
   */
  logoutUser(): Promise<void> {
    const userId: string = firebase.auth().currentUser.uid;
    firebase
    .database()
    .ref(`/userProfile/${userId}`)
    .off();
    return firebase.auth().signOut();
  }

  // stillLogged(): Promise<any>{
  //   return firebase.auth().onAuthStateChanged()
  //   const unsubscribe = firebase.auth().onAuthStateChanged(user => {
  //     if (!user) {
  //       this.rootPage = 'LoginPage';
  //       unsubscribe();
  //     } else {
  //       this.rootPage = HomePage;
  //       unsubscribe();
  //     }
  //   });
  // }

}
