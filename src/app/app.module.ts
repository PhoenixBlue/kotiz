import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { IonicStorageModule } from '@ionic/storage';

import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { Push } from '@ionic-native/push/ngx';
import { Camera } from '@ionic-native/camera';
import { MyApp } from './app.component';
import { FireAuthAPI } from '../providers/fireauth/fireauth';
import { FireFuncAPI } from '../providers/firefunc/firefunc';
import { StoreAPI } from '../providers/store/store';
import { TabBankPage, TabCotisePage, TabSessionPage } from '../pages/pages';

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    MyApp,
    TabBankPage,
    TabCotisePage,
    TabSessionPage
    // other pages... ?
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicStorageModule.forRoot(),
    IonicModule.forRoot(MyApp),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    }),

  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    TabBankPage,
    TabCotisePage,
    TabSessionPage
    // other pages...?
  ],
  providers: [
    Camera,
    Push,
    HttpClientModule,
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    FireAuthAPI,
    FireFuncAPI,
    StoreAPI,
  ]
})
export class AppModule {}
