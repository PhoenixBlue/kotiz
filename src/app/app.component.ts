import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, AlertController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { Push, PushObject, PushOptions } from '@ionic-native/push/ngx';
import firebase from 'firebase';
import { TranslateService } from '@ngx-translate/core';

import { fireConfig } from './environment';
import { FireAuthAPI } from '../providers/fireauth/fireauth';
import { Observable } from 'rxjs';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = 'LoginPage';

  pages: Array<{title: string, component: any, argument:any}>;

  constructor(public platform: Platform,          public statusBar: StatusBar,
              public splashScreen: SplashScreen,  private push: Push,
              public alertCtrl: AlertController,  private translate: TranslateService,
              public fbauth: FireAuthAPI) {
    this.initializeApp();
    var language = window.navigator.language; // To use as default language
    translate.setDefaultLang('en');
    translate.use('fr');
    firebase.initializeApp(fireConfig);
    let titles:any;
    Observable.combineLatest(
      this.translate.get('HOME'),
      this.translate.get('MEMBERS'),
      this.translate.get('SESSIONS'),
      this.translate.get('SEMESTERS'),
      this.translate.get('TONTINES'),
      this.translate.get('PARTICIPANTS'),
      this.translate.get('BANKS'),
      this.translate.get('CHARGES')
    ).subscribe(res => {
      titles = res;
      this.pages = [
        { title:  titles[0], component: 'HomePage',    argument:undefined },
        { title:  titles[1], component: 'MembersPage', argument:undefined },
        { title:  titles[2], component: 'ListPage',    argument:'SESSIONS' },
        { title:  titles[3], component: 'ListPage',    argument:'SEMESTERS' },
        { title:  titles[4], component: 'ListPage',    argument:'TONTINES' },
        { title:  titles[5], component: 'ListPage',    argument:'PARTICIPANTS'},
        { title:  titles[6], component: 'ListPage',    argument:'BANKS' },
        { title:  titles[7], component: 'ListPage',    argument:'CHARGES' },
      ];
    })
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.initPushNotification();


      const unsubscribe = firebase.auth().onAuthStateChanged(user => {
        if (!user) {
          this.rootPage = 'LoginPage';
          unsubscribe();
        } else {
          this.rootPage = 'HomePage';
          unsubscribe();
        }
      });
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component, {objClass:page.argument});
  }

  initPushNotification() {
    if (!this.platform.is('cordova')) {
      console.warn('Push notifications not initialized. Cordova is not available - Run in physical device');
      return;
    }
    const options: PushOptions = {
      android: {
        senderID: '259039759405'
      },
      ios: {
        alert: 'true',
        badge: false,
        sound: 'true'
      },
      windows: {}
    };
    const pushObject: PushObject = this.push.init(options);

    pushObject.on('registration').subscribe((data: any) => {
      console.log('device token -> ' + data.registrationId);
      //TODO - send device token to server
    });

    pushObject.on('notification').subscribe((data: any) => {
      console.log('message -> ' + data.message);
      //if user using app and push notification comes
      if (data.additionalData.foreground) {
        // if application open, show popup
        let confirmAlert = this.alertCtrl.create({
          title: 'New Notification',
          message: data.message,
          buttons: [{
            text: 'Ignore',
            role: 'cancel'
          }, {
            text: 'View',
            handler: () => {
              //TODO: Your logic here
              this.nav.push('HomePage', { message: data.message });
            }
          }]
        });
        confirmAlert.present();
      } else {
        //if user NOT using app and push notification comes
        //TODO: Your logic on click of push notification directly
        this.nav.push('HomePage', { message: data.message });
        console.log('Push notification clicked');
      }
    });

    pushObject.on('error').subscribe(error => console.error('Error with Push plugin' + error));
  }
}
