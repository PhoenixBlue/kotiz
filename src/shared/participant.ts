

export class Participant{
  uid     : string;
  member  : string;
  amount  : number;
  total   : number;
  recvd   : number;
  semester: string;
  session : string;
  comment : string;

  constructor(){
    this.init();
  }

  init(){
    this.uid      = undefined;
    this.member   = '';
    this.amount   = 0 ;
    this.total    = 0 ;
    this.recvd    = 0 ;
    this.semester = '';
    this.session  = '';
    this.comment  = '';

  }

  generateID():boolean{
    if(this.semester.length < 6 || this.member.length < 5 || this.amount <= 0)
      return false;
    
    this.uid = this.semester + 'T' + this.amount + '_' + this.member;

    return true;
  }
}