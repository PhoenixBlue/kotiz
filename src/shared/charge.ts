

export class Charge{
  uid        : string;
  members    : string;
  designation: string;
  amount     : number;
  comment    : string;
  dateCharge : string;
  balance    : number;
  history    : string;
  constructor(){
    this.init();
  }
  /** Initialize object */
  init(){
    this.uid         = undefined;
    this.members     = '';
    this.designation = '';
    this.amount      = 0 ;
    this.comment     = '';
    this.dateCharge  = '';
    this.balance     = 0 ;
    this.history     = '';
  }

}