import { FormControl } from '@angular/forms';

export class myValidator {

  /** Birth date must ensure that the member is older than 10 */
  static ValidBDate(control: FormControl): any {
    var tdate:Date = new Date();
    var yy:number = tdate.getFullYear()-10;
    var mm:number = tdate.getMonth()+1;
    var dd:number = tdate.getDate(); 
    var str = yy + '-' + (mm<10?'0':'') + mm + '-' +
               (dd<10?'0':'') + dd;
    
    if(str.localeCompare(control.value)<0)
      return {"not valid date":true};
    return null;
  }

  static PastDate(control: FormControl):any{
    var tdate:Date = new Date();
    var yy:number = tdate.getFullYear();
    var mm:number = tdate.getMonth()+1;
    var dd:number = tdate.getDate(); 
    var str = yy + '-' + (mm<10?'0':'') + mm + '-' +
               (dd<10?'0':'') + dd;
    
    if(str.localeCompare(control.value)<0)
      return {"not valid date":true};
    return null;
  }

  // static Photo(control: FormControl):any{
  //   var fName:string = control.value;
  //   var ext:string = fName.substr(fName.lastIndexOf('.')+1);
  //   var picExt:string[] = ['jpeg', 'jpg', 'tiff', 'bmp', 'png', 'gif'];
  // }

}