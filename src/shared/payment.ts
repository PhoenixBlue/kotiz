


interface iPayment{
  member : string;
  session: string;
  comment: string;
  amount : number;
}

export class Payment{
  uid : string;
  data: iPayment;

  constructor(){
    this.init();
  }

  init(){
    this.uid  = undefined;
    this.data = undefined;
  }
}