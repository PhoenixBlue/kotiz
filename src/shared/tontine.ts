

export class Tontine{
  uid       : string;
  notes     : string;
  amount    : number;
  active    : boolean;
  tax       : number;

  constructor(){
    this.init();
  }

  init(){
    this.uid     = undefined;
    this.notes   = '';
    this.amount  = 0;
    this.active  = true;
    this.tax     = 0;
  }
}