

export class Semester{
  uid       : string;
  notes     : string;
  total_in  : number;
  total_out : number;
  report    : string;
  num_sessions : number;

  constructor(dateStr?:string){
    this.init();
    if(dateStr!==undefined){
      var semNum = +(dateStr.split('-')[1]) < 7 ? 'S1':'S2'
      this.uid   = dateStr.split('-')[0] + semNum;
    }
  }

  init(){
    this.uid        = undefined;
    this.total_in   = 0;
    this.total_out  = 0;
    this.report     = '';
    this.num_sessions = 0;
  }
}
