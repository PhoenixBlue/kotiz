import { Shared } from "./shared";

export interface iTotal{
  designation : string;   // Name/designation of what we are summing up
  contributed : number;   // Sum of all contributions
  paid_out    : number;   // Sum of everything paid out(collections from members)
  recovered   : number;   // Sum of all monies recovered
}

export interface iCharge{
  uid        : string;
  designation: string;
  amount     : number;
  dateCharge : string;
  balance    : number;
  paid       : number;
  remainder  : number;
};

export interface iSessMember{
  uid   : string;
  details: {
    active    : true;
    fullName  : string;
    gender    : string;
    picURL    : string;
  };
  bank  : {
    paid : number;
    total: number;
  };
  charges : {
    total  : number;
    details: iCharge[];
  },
  tontines: {}
}​​

export class SessionKotiz{
  uid       : string;
  allTotals : iTotal[];
  details   : {
      year    : string;
      semester: {
        uid: string,
        in : number,
        out: number,
        nb_sessions: number
      };
      date    : string;
      report  : string;
      all_In  : number;
      all_out : number;

  };
  members: iSessMember[];
  tontine: any[];

  constructor(dateStr?:string){
    this.init();
    if(dateStr!==undefined){
      var semNum    = +(dateStr.split('-')[1]) < 7 ? 'S1':'S2'
      this.details.semester.uid = dateStr.split('-')[0] + semNum;
      this.uid      = dateStr;
      this.details.date = dateStr;
    }
  }

  init(){
    let curSem = '' + (new Date()).getFullYear() + 'S' + ((new Date()).getMonth()<=5?1:2);

    this.uid        = undefined;
    this.allTotals  = [];
    this.members    = [];
    this.tontine    = [];
    this.details    = {
        year    : curSem.split('S')[0],
        semester: {
          uid: curSem,
          in : 0,
          out: 0,
          nb_sessions: 0
        } ,
        date    : Shared.getCurrentDate(),
        report  : '',
        all_In  : 0,
        all_out : 0
    };
  }

  /** RESET OBJECT'S AMOUNTS TO ZERO
   * ================================ */
  static reset(item:SessionKotiz):SessionKotiz{
    item.details.report = '';
    // Reset totals
    item.allTotals.forEach(oneTotal => {
      oneTotal.contributed = 0;
      oneTotal.paid_out    = 0;
      oneTotal.recovered   = 0;
    })
    item.details.all_In  = 0;
    item.details.all_out = 0;
    // Reset banks and cotisations from members
    item.members.forEach(oneMember => {
      oneMember.bank.paid = 0;
      for(var oneTontine in oneMember.tontines){  // reset tontine contributions
          oneMember.tontines[oneTontine].forEach(onePartP => {
          onePartP.paid = 0;
        })
      }
      // Reset charges
      oneMember.charges.details.forEach(oneCharge => {
        if(oneCharge.paid > 0){
          oneCharge.balance       += oneCharge.paid;
          oneMember.charges.total += oneCharge.paid;
          oneCharge.paid           = 0;
          oneCharge.remainder      = oneCharge.balance;
        }
      })
    })
    return item;
  }

  /** SAVE OBJECT INTO DATABASE
   * ========================== */
  static save(item:SessionKotiz):SessionKotiz {
    // For each member: 
    //   - Update bank if amount>0 
    //      * bank/[year]/transactions/[member uid]/total += amount_bank
    //      * bank/[year]/transactions/[member uid]/transactions/[auto uid]/[amount, date & notes]
    //   - Update cotisations if amount > 0
    //      * participants/[semester uid]/[tontine]/[participant uid]/[comment, member, etc...]
    //      * participants/[semester uid]/[tontine]/[participant uid]/transactions/[new object]
    //
    // For session update
    //   - semesters/[semester uid]/(total_in & total_out)
    //   - semesters/[semester uid]/sessions/[date]/(total_in, total_out, notes)

    return item;
  }

}
