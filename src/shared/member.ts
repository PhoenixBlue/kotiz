


export class Member{
  authID      : string;
  uid         : string;
  surname     : string;
  firstName   : string;
  fullName    : string;
  gender      : string;
  birthDate   : string;
  birthPlace  : string;
  email       : string;
  phone       : string;
  occupation  : string;
  active      : boolean;
  father      : string;
  mother      : string;
  spouse      : string;
  children    : string[];
  addressHome : string;
  addressLocal: string;
  contactHome : string;
  contactLocal: string;
  comment     : string;
  regDate     : string;
  picURL      : string;

  constructor(){
    this.init();
  }

  /** Initialize object */
  init(){
    this.authID       = undefined;
    this.uid          = undefined;
    this.surname      = '';
    this.firstName    = '';
    this.fullName     = '';
    this.gender       = '';
    this.birthDate    = '';
    this.birthPlace   = '';
    this.email        = '';
    this.phone        = '';
    this.occupation   = '';
    this.active       = false;
    this.father       = '';
    this.mother       = '';
    this.spouse       = '';
    this.children     = [];
    this.addressHome  = '';
    this.addressLocal = '';
    this.contactHome  = '';
    this.contactLocal = '';
    this.comment      = '';
    this.regDate      = '';
    this.picURL       = '';
  }
}