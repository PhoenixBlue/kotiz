

interface iCotisation{
  session     : string;
  participant : string;
  amount      : number;
}

export class Cotisation{
  uid : string;
  data: iCotisation;

  constructor(){
    this.init();
  }

  init(){
    this.data = undefined;
    this.uid  = undefined;
  }

}