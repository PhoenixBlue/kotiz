export * from './member';
export * from './semester';
export * from './tontine';
export * from './charge';
export * from './participant';
export * from './session';

export const MAX_IMAGE_SIZE = 150;
export const ROLES = {
  MEMBER     : 'member',
  EXECUTIVE  : 'executive',
  HONORARY   : 'honorary',
  PRESIDENT  : 'president',
  ACCOUNTANT : 'accountant',
  AUDITOR    : 'auditor',
  SECRETARY  : 'secretary',
  CENSOR     : 'censor',
  SPORTS     : 'sports',
  CULTURE    : 'culture',
  WOMEN      : 'women'
};


export class Shared{
  public static classBanks :        string = "BANKS";
  public static classMembers :      string = "MEMBERS";
  public static classCharges :      string = "CHARGES";
  public static classUpdates :      string = "UPDATES";
  public static classSessions :     string = "SESSIONS";
  public static classTontines :     string = "TONTINES";
  public static classSemesters :    string = "SEMESTERS";
  public static classParticipants : string = "PARTICIPANTS";
  public static currentSession:     string = "current_session";
  public static eventUpdateSession: string = "eventUpdateCurrentSession";
  public static eventInitSession:   string = "eventInitializeSession";
  public static eventCloseSession:  string = "eventCloseCurrentSession";
  public static malePicURL :        string = "../../assets/imgs/user_male.png";
  public static femalePicURL :      string = "../../assets/imgs/user_female.png";
  public static updateInterval :    number = 7*24*60*60*1000; // Delay of one week, in milliseconds

  constructor(){}

  public static getCurrentSemester():string{
    let curSem = '' + (new Date()).getFullYear() + 'S' + ((new Date()).getMonth()<=5?1:2);
    return curSem;
  }

  public static getCurrentDate():string{
    const da = new Date();
    const yy = String(da.getFullYear());
    const mm = da.getMonth()+1;
    const dd = da.getDate();
    return yy + '-' + (mm>9? '':'0') + String(mm) + '-' + (dd>9? '':'0') + String(dd);
  }
}

export class CustomDate {
  private valueString: string;
  private valueDate: Date;

  constructor(){
    this.valueDate   = new Date();
    this.valueString = this.valueDate.toISOString();
  }

  setISODate(dd:Date){
    this.valueDate   = dd;
    this.valueString = this.valueDate.toISOString();
    return this;
  }

  setLocalDate(dd:Date){
    this.valueDate   = new Date(dd.getTime() + dd.getTimezoneOffset()*60000);
    this.valueString = this.valueDate.toISOString();
    return this;
  }

  setISOString(ss:string){
    this.valueString = ss;
    this.valueDate   = new Date(ss);
    return this;
  }

  setLocalString(ss:string){
    let dd = new Date(ss);
    this.setLocalDate(dd);
    return this;
  }

  getISODate(){
    return this.valueDate;
  }

  getISOString(){
    return this.valueString;
  }

  getLocalString(){
    return this.getLocalDate().toISOString();
  }

  getLocalDate(){
    return new Date(this.valueDate.getTime() - this.valueDate.getTimezoneOffset()*60000);
  }
  
}

export interface cloudUpdates{
  lastUpdate  : string, 
  banks       : string,
  charges     : string,
  members     : string,
  participants: string,
  roles       : string,
  semesters   : string,
  tontines    : string
}