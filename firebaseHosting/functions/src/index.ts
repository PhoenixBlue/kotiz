import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';

admin.initializeApp();
const fsDB    = admin.firestore();
// const fStore  = admin.storage(); 
// Root authentication identifiers
const root_auth ={
  uid: 'bRDZW4reeCWUJuhVqgYfxoaiZn43',
  email: 'armas.pretoria@gmail.com'
}
// const TAG = {
//   semester      : 'semester',
//   members       : 'members',
//   bank          : 'bank',
//   transactions  : "transactions",
//   updates       : "updates",
//   tontines      : "tontines",
//   participants  : "participants"
// }

const ROLES = {
  root    : "root",
  member  : "member",
  SG      : "secretary",
  Pdt     : "president",
  exec    : "executive",
  CC      : "accountant"
}

enum STATUS {
  fail = -1,
  ok,
  permission_denied
}

/** Create account */
// exports.createAccount = 

/** Create / Update a member
 * ========================== */
exports.updateMember = functions.https.onCall( (data, context) => {
  // root member and secretaries are allowed to update members
  const authParams:any = {
    uid:      context.auth.uid,
    email:    context.auth.token.email,
    // memberID: data.uid
  };
  // Check if user is allowed to perform operation
  return checkPermission(authParams, [ROLES.root, ROLES.SG]).then(() => {
    // set object to add/ update
    const member:any = data;
    // Check if uid of member object is present 
    let fullRef : admin.firestore.DocumentReference;
    let headRef : admin.firestore.DocumentReference;
    const countRef  = fsDB.collection('counters').doc('count_members');
    let createNewMember = false;
    if(member.uid!==undefined && member.uid!==null){ // Basically for updates
      fullRef  = fsDB.collection('members_full').doc(member.uid);
      headRef  = fsDB.collection('members_header').doc(member.uid);
      if(member.create===true) // New item must be created despite uid present
        createNewMember = true;
    } else { // Creation of new member, when uid not provided
      fullRef  = fsDB.collection('members_full').doc();
      headRef  = fsDB.collection('members_header').doc(fullRef.id.toString());
      createNewMember = true;
    }

    // Transaction operations
    // Picture upload is handled on the client side.
    return fsDB.runTransaction(t => {
      return t.get(fullRef).then( doc1 => {
        headRef = fsDB.collection('members_header').doc(doc1.id);
        return t.get(headRef).then(doc => {
          let incCounter = 0;
          if(!createNewMember && !doc.exists){ // Error, cannot update if not yet exists
            return t;
          }
          
          if(createNewMember){
            incCounter = 1;
          }
        return t
            .set(headRef, { // Set header
              fullName    : member.fullName    ,
              gender      : member.gender      ,
              active      : member.active      ,
              picURL      : member.picURL      ,     })
            .set(fullRef, { // Set full details
              surname     : member.surname     ,
              firstName   : member.firstName   ,
              birthDate   : member.birthDate   ,
              birthPlace  : member.birthPlace  ,
              email       : member.email       ,
              phone       : member.phone       ,
              occupation  : member.occupation  ,
              father      : member.father      ,
              mother      : member.mother      ,
              spouse      : member.spouse      ,
              children    : member.children    ,
              addressHome : member.addressHome ,
              addressLocal: member.addressLocal,
              contactHome : member.contactHome ,
              contactLocal: member.contactLocal,
              comment     : member.comment     ,
              regDate     : member.regDate     ,     })
              // Update counter if needed
            .update(countRef, {members_count: admin.firestore.FieldValue.increment(incCounter)});
        }).catch( err => {
          return {
            status: STATUS.fail,
            message: err.message,
            error: err,
            pointer: 't.get2'
          }
        });
      }).then(() => {
        return { status : STATUS.ok , pointer:'then t.get'}
      }).catch(err => {
        return {
          status: STATUS.fail,
          message: err.message,
          error: err,
          pointer: 't.get'
        }
      });
    }).then(() => {
      return { status : STATUS.ok, pointer: 'then run transact' }
    }).catch(error =>{
      return {
        status: STATUS.fail,
        message: error.message,
        pointer:  'run transaction err',
        error: error
      }
    });
  }).catch(err => {
    return {
      status:   STATUS.fail,
      message:  err.message,
      pointer:  'check permission err',
      error:    err
    }
  });
});

/** Request list of members:
 * ==========================
 * The request contains some parameters to filter the members we want:
 * Filters are range of records, gender and activity of members.
 * -------------------------------------------------------------------- */
exports.getMembers = functions.https.onCall((data, context) => {
  const conf   = data;
  const authParams:any = {
    uid:      context.auth.uid,
    email:    context.auth.token.email,
    // memberID: data.uid
  };
  // Check if user is allowed to perform operation
  return checkPermission(authParams, [ROLES.root, ROLES.exec]).then( () => {
    // Get list of members

    // Get sorted members up to 'Limit'
    let direction = 1; // Ascending
    let mRef = fsDB.collection('members_header').orderBy('fullName');
    if(conf.range.startValue!==undefined && conf.range.startValue!==null)
      mRef = mRef.startAfter(conf.range.startValue);
    else if(conf.range.endValue!==undefined && conf.range.endValue!==null){
      direction = -1;
      if(conf.range.endValue.toUpperCase().localeCompare('ZZZZZ')==0){ // Last items
        mRef = fsDB.collection('members_header').orderBy('fullName', 'desc');
      } else {
        mRef = mRef.endBefore(conf.range.endValue);
      }
    }
    mRef = mRef.limit(conf.range.limit);
    let members:any[] = [];
    
    return filterMembersCallback(conf, members, direction, undefined).then((result) => {
      return {
        status:   STATUS.ok,
        data:     result,
        pointer:  'get',
      }
    }).catch(err => {
      return {
        status:   STATUS.fail,
        message:  err.message,
        pointer:  'check get err',
        error:    err
      }
    }) ;
  }).catch(err => {
    return {
      status:   STATUS.fail,
      message:  err.message,
      pointer:  'check permission err',
      error:    err
    }
  });
});

/** Delete a member from the firestore database. NOT YET TESTED
 * ============================================================= */
exports.deleteMember = functions.https.onCall((data, context) => {
  // const data   = data;
  const authParams:any = {
    uid:      context.auth.uid,
    email:    context.auth.token.email,
  };
  const headRef = fsDB.collection('members_header').doc(data);
  const fullRef = fsDB.collection('members_full').doc(data);
  const countRef  = fsDB.collection('counters').doc('count_members');
  return checkPermission(authParams, ['root', 'executive']).then( () => {
    return fsDB.runTransaction(t => {
      return t.get(headRef).then(() => {
        return t
          .delete(headRef)
          .delete(fullRef)
          .update(countRef, {members_count: admin.firestore.FieldValue.increment(-1)});
      }).catch(err1 => {
        return {
          status:   STATUS.fail,
          message:  err1.message,
          pointer:  'exec transaction err',
          error:    err1
        }
      })
    })
  }).catch(err => {
    return {
      status:   STATUS.fail,
      message:  err.message,
      pointer:  'check permission err',
      error:    err
    }
  });;
});

/** Get full details of a particular member
 * ========================================= */
exports.getFullMember = functions.https.onCall((data, context) => {
  const authParams:any = {
    uid:      context.auth.uid,
    email:    context.auth.token.email,
  };
  const fullRef = fsDB.collection('members_full').doc(data);
  return checkPermission(authParams, ['root', 'executive', 'member']).then( () => {
    return fullRef.get().then(result =>{
      return {
        status:   STATUS.ok,
        data:     result.data(),
        pointer:  'get1Member',
      }
    }).catch(err1 =>{
      return {
        status:   STATUS.fail,
        message:  err1.message,
        pointer:  'get 1 member err',
        error:    err1
      }
    })
  }).catch(err => {
    return {
      status:   STATUS.fail,
      message:  err.message,
      pointer:  'check permission err',
      error:    err
    }
  });

});


/** Insert new semester in our database
 * ======================================  */
exports.updateSemester = functions.https.onCall((data, context) => {
  const authParams:any = {
    uid:      context.auth.uid,
    email:    context.auth.token.email,
  };
  // set object to add/ update
  const item:any = data;
  // Check if uid of member object is present 
  let fullRef : admin.firestore.DocumentReference;
  const createItem = data.create?true:false;

  if(item.uid!==null && item.uid!==undefined){
    fullRef = fsDB.collection('semesters').doc(item.uid);
  } else {
    return new Promise((resolv, reject) => {
      reject({
        status:   STATUS.fail,
        message:  'uid not set',
        pointer:  'check uid err',
        error:    undefined
      })
    });
  }

  return checkPermission(authParams, ['root', 'executive']).then(() => {
    return fsDB.runTransaction(t => {
      return t.get(fullRef).then(docFull => {
        if(createItem && docFull.exists){
          return t;
        }
        return t.set(fullRef, {
            total_in:   item.total_in,
            total_out:  item.total_out,
            report:     item.report
          }, {merge: true});
      }).then(() => {return { status : STATUS.ok , pointer:'then t.get'};
      }).catch(err1 => {
        return {
          status:   STATUS.fail,
          message:  err1.message,
          error:    err1,
          pointer:  't.get1'
        }
      });
    }).then(() => {
      return { status : STATUS.ok, pointer: 'then run transact' }
    }).catch(error =>{
      return {
        status:   STATUS.fail,
        message:  error.message,
        pointer:  'run transaction err',
        error:    error
      }
    });
  }).catch(err => {
    return {
      status:   STATUS.fail,
      message:  err.message,
      pointer:  'check permission err',
      error:    err
    }
  });
});


/** Insert new simple item in our database
 * ========================================= 
 * Insert/update in collections 'semester' or 'tontines'
 * Request must specify the ID and collection as 'table'
 * 
 * -------------------------------------------------------------- */
exports.updateSimpleCollection = functions.https.onCall((data, context) => {
  const authParams:any = {
    uid:      context.auth.uid,
    email:    context.auth.token.email,
  };
  // set object to add/ update
  const item:any  = data;
  // Check if uid of member object is present 
  const createItem  = data.create?true:false;
  let fullRef : admin.firestore.DocumentReference;

  if(item.uid!==null && item.uid!==undefined && 
    item.table!==null && item.table!==undefined){
    fullRef = fsDB.collection(item.table.toLowerCase()).doc(item.uid);
  } else {
    return new Promise((resolv, reject) => {
      reject({
        status:   STATUS.fail,
        message:  'uid or table not set',
        pointer:  'check uid err',
        error:    undefined
      })
    });
  }
  return checkPermission(authParams, ['root', 'executive']).then(() => {
    return fsDB.runTransaction(t => {
      return t.get(fullRef).then(docFull => {
        if(createItem && docFull.exists){
          return t;
        }
        switch(item.table.toLowerCase()){
          case 'semesters':{
            return t.set(fullRef, {
              total_in:   item.total_in,
              total_out:  item.total_out,
              report:     item.report,
              num_sessions: item.num_sessions
            });
          }
          case 'tontines':{
            return t.set(fullRef, {
              active:     item.active,
              tax:        item.tax,
              notes:      item.notes
            });
          }
          default:{
            return t;
          }
        }
      }).then(() => {return { status : STATUS.ok , pointer:'then t.get'};
      }).catch(err1 => {
        return {
          status:   STATUS.fail,
          message:  err1.message,
          error:    err1,
          pointer:  't.get1'
        }
      });
    }).then(() => {
      return { status : STATUS.ok, pointer: 'then run transact' }
    }).catch(error =>{
      return {
        status:   STATUS.fail,
        message:  error.message,
        pointer:  'run transaction err',
        error:    error
      }
    });
  }).catch(err => {
    return {
      status:   STATUS.fail,
      message:  err.message,
      pointer:  'check permission err',
      error:    err
    }
  });
});

/** Request list of items in a collection 
 * ====================================== */
exports.getListIDs = functions.https.onCall((data, context) => {
  const authParams:any = {
    uid:      context.auth.uid,
    email:    context.auth.token.email,
  };
  const table = data;
  const allowedRoles:string[] = ['root'];
  switch(table.toUpperCase()){
    case 'MEMBERS':{ // Must be executive to request list of members
      allowedRoles.push('executive');
      break;
    }
    default:{
      allowedRoles.push('member');
    }
  }

  return checkPermission(authParams, allowedRoles).then(() => {
    return fsDB.collection(table).get().then(snapshot => {
      let allItems:any[] = [];
      snapshot.forEach(snap => {
        allItems.push(snap.id);
      });
      return {
        status:   STATUS.ok,
        data:     JSON.stringify(allItems),
        pointer:  'getIDs',
      }
    }).catch(err1 => {
      return {
        status: STATUS.fail,
        message: err1.message,
        pointer:  'run transaction err',
        error: err1
      }
    });
  }).catch(err => {
    return {
      status:   STATUS.fail,
      message:  err.message,
      pointer:  'check permission err',
      error:    err
    }
  });

});



/** Update "update dates" in the cloud database
 * ============================================ */
exports.setUpdates = functions.https.onCall((data, context) => {
  const authParams:any = {
    uid:      context.auth.uid,
    email:    context.auth.token.email,
  };

  return checkPermission(authParams, ['root', 'member']).then(() => {
    const updateRef = fsDB.collection('updates');
    const batch     = fsDB.batch();

    if(data.banks       !== undefined) {
      batch.set(updateRef.doc('banks'), {date: data.banks}, {merge: true})
    }
    if(data.charges     !== undefined){
      batch.set(updateRef.doc('charges'), {date: data.charges}, {merge: true})
    }
    if(data.members     !== undefined){
      batch.set(updateRef.doc('members'), {date: data.members}, {merge: true})
    }
    if(data.participants!== undefined){
      batch.set(updateRef.doc('participants'), {date: data.participants}, {merge: true})
    }
    if(data.roles       !== undefined){
      batch.set(updateRef.doc('roles'), {date: data.roles}, {merge: true})
    }
    if(data.semesters   !== undefined){
      batch.set(updateRef.doc('semesters'), {date: data.semesters}, {merge: true})
    }
    if(data.tontines    !== undefined){
      batch.set(updateRef.doc('tontines'), {date: data.tontines}, {merge: true})
    }
    return batch.commit();
  }).catch(err => {
    return {
      status:   STATUS.fail,
      message:  err.message,
      pointer:  'check permission err',
      error:    err
    }
  });
});

/** Request list of update times 
 * ============================= */
exports.getUpdates = functions.https.onCall((data, context) => {
  const authParams:any = {
    uid:      context.auth.uid,
    email:    context.auth.token.email,
  };
  const allowedRoles:string[] = ['root', ROLES.member];

  return checkPermission(authParams, allowedRoles).then(() => {
    let ref:any;
    ref = fsDB.collection('updates');
    // If argument specified, only get that specific update
    if(data !== undefined && data !== null){
      ref = ref.doc(data);
    }

    return ref.get().then(snapshot => {
      let allItems:any = {}
      
      if(data !== undefined && data !== null){ 
        allItems[data] = snapshot.data().date;
      } else {  // If no argument, retrieve all updates
        snapshot.forEach(snap => {
          allItems[snap.id] = snap.data().date;
        });
      }
      return {
        status:   STATUS.ok,
        data:     allItems,
        pointer:  'getUpdates',
      }
    }).catch(err1 => {
      return {
        status: STATUS.fail,
        message: err1.message,
        pointer:  'run transaction err',
        error: err1
      }
    });
  }).catch(err => {
    return {
      status:   STATUS.fail,
      message:  err.message,
      pointer:  'check permission err',
      error:    err
    }
  });

});

/** Request details of a single document in a collection
 * ======================================================
 * This function  therefore requires the collection name 
 * and the uid of the document to extract.
 * 
 * -------------------------------------------------------- */
exports.getItemDetails = functions.https.onCall((data, context) => {
  const authParams:any = {
    uid:      context.auth.uid,
    email:    context.auth.token.email,
  };

  let fullRef : admin.firestore.DocumentReference;
  const item = data;

  if(item.uid!==null && item.uid!==undefined && 
    item.table!==null && item.table!==undefined){
    fullRef = fsDB.collection(item.table.toLowerCase()).doc(item.uid);
  } else {
    return new Promise((resolv, reject) => {
      reject({
        status:   STATUS.fail,
        message:  'uid or table not set',
        pointer:  'check uid err',
        error:    item
      })
    });
  }

  return checkPermission(authParams, ['root', 'member'] ).then(() => {
    return fullRef.get().then(res => {
      return {
        status:   STATUS.ok,
        data:     res.data(),
        pointer:  'getIDs',
      }
    }).catch(err1 => {
      return {
        status: STATUS.fail,
        message: err1.message,
        pointer:  'run transaction err',
        error: err1
      }
    })
  }).catch(err => {
    return {
      status:   STATUS.fail,
      message:  err.message,
      pointer:  'check permission err',
      error:    err
    }
  });
});



/** Insert new participant in our database
 * ========================================= 
 * Insert/update in collections 'participant'
 * Documents ID are semester id, documents fields
 * are actually sub-collections named after 
 * tontines' id. Then, subsequent documents are
 * the actual participants details.
 * 
 * -------------------------------------------------- */
exports.updateParticipant = functions.https.onCall((data, context) => {
  const authParams:any = {
    uid:      context.auth.uid,
    email:    context.auth.token.email,
  };
  // set object to add/ update
  const item:any  = data;
  // Check if uid of member object is present 
  if(item.uid===null || item.uid===undefined){
    return new Promise((resolve, reject) => {
      reject({
        status:   STATUS.fail,
        message:  'uid or table not set',
        pointer:  'check uid err',
        error:    undefined
      })
    });
  }

  return checkPermission(authParams, ['root', 'accountant']).then(() => {
    return getValidParticipantID(item, 0).then(validRef => {
      // Found a valid ID
      return fsDB.runTransaction(t => {
        return t.get(validRef).then(doc =>{
          return t.set(validRef, {
            member  : item.member,
            total   : item.total,
            recvd   : item.recvd,
            session : item.session,
            comment : item.comment,
          })
        }).catch(err2 => {
          return {
            status:   STATUS.fail,
            message:  err2.message,
            pointer:  'get valid ref',
            error:    err2
          }
        })
      }).then (() => {
        return { status : STATUS.ok, pointer: 'then run transact' }
      }).catch(error =>{
        return {
          status:   STATUS.fail,
          message:  error.message,
          pointer:  'run transaction err',
          error:    error
        }
      })
    }).catch(err1 => {
      return {
        status:   STATUS.fail,
        message:  err1.message,
        pointer:  'check getting reference',
        error:    err1
      }
    })
  }).catch(err => {
    return {
      status:   STATUS.fail,
      message:  err.message,
      pointer:  'check permission err',
      error:    err
    }
  });
});


/** Retrieve participants, from a specific semester
 * =================================================
 * Participants can only be requested for a specific 
 * semester, and for all tontines!
 * 
 * -------------------------------------------------- */
exports.getParticipants = functions.https.onCall((data, context) => {
  const authParams:any = {
    uid:      context.auth.uid,
    email:    context.auth.token.email,
  };
  // set object to add/ update
  // const item:any  = data;
  // Check if uid of member object is present 
  if(data===null || data===undefined){
    return new Promise((resolv, reject) => {
      reject({
        status:   STATUS.fail,
        message:  'semester not set',
        pointer:  'check semester err',
        error:    undefined
      })
    });
  }

  const semRef = fsDB.collection('participants').doc(data);
  // let allParticipants:any[] = [];
  let allParticipants:any  = {};

  return checkPermission(authParams, ['root', 'accountant']).then(() => {
    return semRef.listCollections().then(tontinesCol => {
      let allTontines:any[]=[];
      tontinesCol.forEach(tontine =>{
        allTontines.push(tontine.get());
      })
      return Promise.all(allTontines).then(allSnapshots => {
        allSnapshots.forEach(snapshot => {
          snapshot.forEach(partP => {
            const item = partP.data();
            item.uid = partP.id;
            // allParticipants.push(item);
            const tontVal:string  = item.uid.split('_')[0].split('T')[1];
            // let   item2 = item;
            if(allParticipants[item.member]===null || allParticipants[item.member]===undefined){
              allParticipants[item.member] = {};
            }
            if(allParticipants[item.member][tontVal] === null || allParticipants[item.member][tontVal]===undefined){
              allParticipants[item.member][tontVal] = [];
            }
            const item2 = {
              comment : item.comment,
              recvd   : item.recvd,
              session : item.session,
              total   : item.total,
              uid     : item.uid
            }
            allParticipants[item.member][tontVal].push(item2)
          })
        })
        return {
          status:   STATUS.ok,
          data:     allParticipants,
          // xtra:     allParticipants2,
          pointer:  'get participants',
        }
      }).catch(err2 => {
        return {
          status:   STATUS.fail,
          message:  err2.message,
          pointer:  'get all participants err',
          error:    err2
        }
      })
    }).catch(err1 => {
      return {
        status:   STATUS.fail,
        message:  err1.message,
        pointer:  'get tontine collections err',
        error:    err1
      }
    });
  }).catch(err => {
    return {
      status:   STATUS.fail,
      message:  err.message,
      pointer:  'check permission err',
      error:    err
    }
  });

});



/** Insert / Update a new charge
 * =============================
 * The charge must have amount, designation, comment and member(s)
 * Also, must specify if amount is global(to be divided by all active members)
 * or individual (amount for each member). balance is of course set to the 
 * value of amount to pay, date is set to current date
 */

 exports.updateCharge = functions.https.onCall((data, context) => {
  const authParams:any = {
    uid:      context.auth.uid,
    email:    context.auth.token.email,
  };

  return checkPermission(authParams, ['root', 'executive']).then(() => {
    let requests:any[]=[];//  Promise<any>;
    let membersID:any[] = [];
    let chargeAmount = +data.amount;
    // If charge must be applied to all active members
    if(data.singleAmount!==true || data.allMembers===true){
      const conf = {
        range:{
          limit: 10000,
        },
        filter:{
          active: true,
        }
      }
      let allMembers:any[] = [];
      requests[0] = fetchCharges({full:false, allMembers:true, member:[]});
      requests[1] = filterMembersCallback(conf, allMembers, 1, undefined);
    } // else apply single amount to all selected members
    else {
      requests[0] = fetchCharges({full:false, allMembers:false, member:membersID});
      requests[1] = new Promise<any>((resolve, reject) => {resolve(data.members)});
    }
    return Promise.all(requests).then(reply => {
      const responses:any[] = reply;
      // membersID = ids;
      membersID = [];
      if(data.singleAmount!==true || data.allMembers===true){
        responses[1].forEach(element => {
          membersID.push({uid:element.uid})
        });
        if(data.singleAmount!==true){
          chargeAmount = Math.ceil(data.amount / membersID.length);
        }
      } else {
        responses[1].forEach(element => {
          membersID.push({uid:element})
        });
      }
      const previousCharges:any[] = responses[0];
      const currCharges: any[]    = membersID;
      membersID = [];
      currCharges.forEach(elt => {
        elt.total = 0;
        previousCharges.forEach(pCharge => {
          if(elt.uid.localeCompare(pCharge.uid)==0){
            elt.total = pCharge.total;
            return
          }
        })
        membersID.push(elt);
      })

      const chargeRef = fsDB.collection('charges');
      return fsDB.runTransaction(t => {
        return t.get(chargeRef.doc()).then(snapshot => {
          membersID.forEach(elt => {
            t.set(chargeRef.doc(elt.uid).collection('allcharges').doc(), {
              // member: snap.get,
              amount:       chargeAmount,
              date:         data.date,
              balance:      chargeAmount,
              comment:      data.comment,
              designation:  data.designation
            }).set(chargeRef.doc(elt.uid), {total: chargeAmount+(elt.total)}, {merge:true})
          })
          return t;
        }).catch(err2 => {
          return {
            status:   STATUS.fail,
            message:  err2.message,
            pointer:  'get transact err',
            error:    err2
          }   
        })
      }).catch(err1 => {
        return {
          status:   STATUS.fail,
          message:  err1.message,
          pointer:  'run transact err',
          error:    err1
        }
      });
    }).catch(err1 => {
      return {
        status:   STATUS.fail,
        message:  err1.message,
        pointer:  'get members err',
        error:    err1
      }
    })
  }).catch(err => {
    return {
      status:   STATUS.fail,
      message:  err.message,
      pointer:  'check permission err',
      error:    err
    }
  });
 });


 /** GET CHARGES LIST
  * =================
  * The request must indicate if only total needed (full==false)
  * details of all charges (full==true). Also specify list of 
  * members whose info are needed.
  */
exports.getChargesDetails = functions.https.onCall((data, context) => {
  const authParams:any = {
    uid:      context.auth.uid,
    email:    context.auth.token.email,
  };

  if(data===null || data===undefined){
    return new Promise((resolv, reject) => {
      reject({
        status:   STATUS.fail,
        message:  'data not set',
        pointer:  'check data err',
        error:    undefined
      })
    });
  }

  return checkPermission(authParams, ['root', 'member']).then(() => {

    return fetchCharges(data).then(res => {
      return {
        status:   STATUS.ok,
        data:     res,
        pointer:  'fetch charges',
      }

    }).catch(err => {
      return {
        status:   STATUS.fail,
        message:  err.message,
        pointer:  'fetch charges err',
        error:    err
      }    
    })
  }).catch(err => {
    return {
      status:   STATUS.fail,
      message:  err.message,
      pointer:  'check permission err',
      error:    err
    }
  });
});


/** UPDATE / INSERT BANK TRANSACTION
 * ================================= */

 exports.updateBank = functions.https.onCall((data, context) => {
  const authParams:any = {
    uid:      context.auth.uid,
    email:    context.auth.token.email,
  };

  return checkPermission(authParams, ['root', 'accountant']).then(() => {
    // data.transactions is array of objects: {
    //                     member: memberuid
    //                     amount: amount in transaction
    //                        uid: transaction id
    //                       note: any comment?
    //                            }
    // data.date is date of transactions
    // This is pure writing! no reading! the client application must
    // actually provide all needed info to just perform writing.
    const bankRef   = fsDB.collection('bank');
    const batch     = fsDB.batch();
    const yearRef   = bankRef.doc(data.year);
    const colRef    = yearRef.collection('transactions');
    // const ref1 = '';
    data.transactions.forEach(trans => {
      let memRef = colRef.doc(trans.member);
      batch.set(memRef.collection('transactions').doc(),{
        date: data.date,
        amount: trans.amount,
        notes: trans.notes
      });
      batch.set(memRef, {total: admin.firestore.FieldValue.increment(trans.amount)}, {merge: true})
      batch.set(yearRef, {total: admin.firestore.FieldValue.increment(trans.amount)}, {merge: true})
    });
    return batch.commit();
  }).catch(err => {
    return {
      status:   STATUS.fail,
      message:  err.message,
      pointer:  'check permission err',
      error:    err
    }    
  })
 });


/** GET BANK PARTICIPANTS & DETAILS
 * =================================
 * Following what has been done for charges, the function must be 
 * able to return all (or many) bank participants with their total
 * contribution only, or all transactions of a single participant.
 * 
 * ----------------------------------------------------------------- */
exports.getBankDetails = functions.https.onCall((data, context) => {
  const authParams:any = {
    uid:      context.auth.uid,
    email:    context.auth.token.email,
  };

  if(data===null || data===undefined){
    return new Promise((resolv, reject) => {
      reject({
        status:   STATUS.fail,
        message:  'data not set',
        pointer:  'check data err',
        error:    undefined
      })
    });
  }
  
  return checkPermission(authParams, ['root', 'member']).then(() => {
    return fetchBankDetails(data).then(res => {
      return {
        status:   STATUS.ok,
        data:     res,
        pointer:  'fetch bank details',
      }
    }).catch(err => {
      return {
        status:   STATUS.fail,
        message:  err.message,
        pointer:  'fetch bank details err',
        error:    err
      }    
    })
  }).catch(err => {
    return {
      status:   STATUS.fail,
      message:  err.message,
      pointer:  'check permission err',
      error:    err
    }
  });
});
   

/** Get list of sessions
 * ===================== */
 exports.getSessions = functions.https.onCall((data, context) => {
  const authParams:any = {
    uid:      context.auth.uid,
    email:    context.auth.token.email,
  };

  if(data===null || data===undefined || data.semester===undefined){
    return new Promise((resolv, reject) => {
      reject({
        status:   STATUS.fail,
        message:  'data not set',
        pointer:  'check data err',
        error:    undefined
      })
    });
  }
  
  return checkPermission(authParams, ['root', 'member']).then(() => {
    const sessRef = fsDB.collection('semesters').doc(data.semester).collection('sessions');
    let sessRequest: Promise<any>;
    if(data.date!==undefined){
      sessRequest = sessRef.doc(data.date).get();
    } else {
      sessRequest = sessRef.get();
    }
    return sessRequest.then(snapshot => {
      if(data.date!==undefined){
        return{
          status:   STATUS.ok,
          data:     snapshot.data(),
          pointer:  'get sessions',
        }
      } else {
        let allSessions : any[] = [];
        snapshot.forEach(snap => {
          let item = snap.data();
          item.uid = snap.id;
          allSessions.push(item)
        })
        return {
          status:   STATUS.ok,
          data:     allSessions,
          pointer:  'get sessions',
        }
      }
    }).catch(err1 => {
      return {
        status:   STATUS.fail,
        message:  err1.message,
        pointer:  'get sessions err',
        error:    err1
      }
    })
  }).catch(err => {
    return {
      status:   STATUS.fail,
      message:  err.message,
      pointer:  'check permission err',
      error:    err
    }
  })
 })


/** SAVE SESSION
 * ============= */
exports.saveSession = functions.https.onCall((data, context) => {
  const authParams:any = {
    uid:      context.auth.uid,
    email:    context.auth.token.email,
  };

  if(data===null || data===undefined){
    return new Promise((resolv, reject) => {
      reject({
        status:   STATUS.fail,
        message:  'data not set',
        pointer:  'check data err',
        error:    undefined
      })
    });
  }

  return checkPermission(authParams, ['root', 'accountant']).then(() => {
    const bankMainRef   = fsDB.collection('bank').doc(''+data.details.year);
    const bankTransRef  = bankMainRef.collection("transactions");
    const partPMainRef  = fsDB.collection('participants').doc(data.details.semester.uid);
    const semRef        = fsDB.collection('semesters').doc(data.details.semester.uid);
    const sessionRef    = semRef.collection('sessions').doc(data.details.date);
    const chargeRef     = fsDB.collection('charges');
    const batch         = fsDB.batch();
    // Must get total banks, total semester in and out
    
    // For each member, update bank:
    data.members.forEach(oneMember => {
      if(oneMember.bank.paid > 0){
        const oneBankRef      = bankTransRef.doc(oneMember.uid);
        const oneBankTransRef = oneBankRef.collection('transactions').doc(data.details.date);
        const oneBankObject   = {amount: oneMember.bank.paid}; 
        batch.set(oneBankRef,       {total:oneMember.bank.total}, {merge: true});
        batch.set(oneBankTransRef,  oneBankObject,                {merge: true});
      }
      // For each member, update charges:
      const oneChargeRef    = chargeRef.doc(oneMember.uid);
      let totalUpdated:boolean = false;
      oneMember.charges.details.forEach(oneCharge => {
        const chargeSave:any = {
          uid         : oneCharge.uid,
          amount      : oneCharge.amount,
          balance     : oneCharge.remainder,
          date        : oneCharge.dateCharge,
          designation : oneCharge.designation,
        }
        if(oneCharge.uid === undefined || (oneCharge.balance > oneCharge.remainder) ){
          const allChargesRef  = oneChargeRef.collection('allcharges');
          const temp_ChargeRef = (oneCharge.uid===undefined) ? allChargesRef.doc(oneCharge.uid) : allChargesRef.doc();
          batch.set(temp_ChargeRef, chargeSave, {merge: true} );
          totalUpdated = true;
        }
      });
      if(totalUpdated){
        batch.set(oneChargeRef, {total:oneMember.charges.total}, {merge: true} );
      }
      // For each member, update tontines:
      for(const oneTontine in oneMember.tontines){
        const oneTontRef = partPMainRef.collection(oneTontine); // Reference to the "tontine"
        oneMember.tontines[oneTontine].forEach(onePartP => {
          const onePartPRef   = oneTontRef.doc(onePartP.uid);
          const oneContribRef = onePartPRef.collection('transactions').doc(data.details.date);
          const onePartPObj   = {
            member    : oneMember.uid,
            recvd     : onePartP.recvd,
            session   : onePartP.session,
            total     : onePartP.total,
          }
          batch.set(onePartPRef,    onePartPObj,            {merge: true});
          batch.set(oneContribRef,  {amount:onePartP.paid}, {merge: true});
        });
      }
    });
    
    const semObject = {
      total_in : data.details.semester.in,
      total_out: data.details.semester.out,
      num_sessions: data.details.semester.nb_sessions,
    };
    const sessObj   = {
      total_in : data.details.all_In,
      total_out: data.details.all_out,
    };
    // For the whole session, update semester's details
    batch.set(semRef,     semObject, {merge: true});
    // For the whole session, update session's details
    batch.set(sessionRef, sessObj,   {merge: true});
    // Increment total bank for the year
    const totalBank     = data.allTotals[0];
    const bankIncrement =  admin.firestore.FieldValue.increment(totalBank.contributed + totalBank.recovered - totalBank.paid_out);
    batch.update(bankMainRef, {total:bankIncrement});

    return batch.commit().then(res => {
      return new Promise((resolve2, reject2) => {
        resolve2({
          status:   STATUS.ok,
          data:     data,
          res:      res // TODO: remove on final stuff, to reduce data sent over network
        });
      })
    }).catch(err => {
      return new Promise((resolve2, reject2) => {
        reject2({
          status:   STATUS.fail,
          message:  err.message,
          pointer:  'batch commit fail',
          error:    err
        });
      })
    });
  }).catch(err => {
    return {
      status:   STATUS.fail,
      message:  err.message,
      pointer:  'check permission err',
      error:    err
    }    
  })
})

/** Request list of items in a collection 
 * ====================================== */
exports.getListItems = functions.https.onCall((data, context) => {
  const authParams:any = {
    uid:      context.auth.uid,
    email:    context.auth.token.email,
  };
  const table = data;
  const allowedRoles:string[] = ['root'];
  switch(table.toUpperCase()){
    case 'MEMBERS':{ // Must be executive to request list of members
      allowedRoles.push('executive');
      break;
    }
    default:{
      allowedRoles.push('member');
    }
  }

  return checkPermission(authParams, allowedRoles).then(() => {
    return fsDB.collection(table).get().then(snapshot => {
      let allItems:any[] = [];
      snapshot.forEach(snap => {
        let obj = snap.data();
        obj.uid = snap.id;
        allItems.push(obj);
      });
      return {
        status:   STATUS.ok,
        data:     JSON.stringify(allItems),
        pointer:  'getIDs',
      }
    }).catch(err1 => {
      return {
        status: STATUS.fail,
        message: err1.message,
        pointer:  'run transaction err',
        error: err1
      }
    });
  }).catch(err => {
    return {
      status:   STATUS.fail,
      message:  err.message,
      pointer:  'check permission err',
      error:    err
    }
  });


});



/** GENERAL FUNCTION TO ADD / UPDATE A RECORD
 * ===========================================
 * The data argument must specify the collection to modify,
 * the details of object to update, also specify the type
 * of operation (insertion or update)
 */


/***************************************************************************
 ************  ALL BELOW ARE FUNCTIONS USED FOR MODULARITY   ***************
 *************************************************************************** /

/** Check if authenticated user's roles are among the ones allowed 
 * ================================================================ */
function checkPermission(authParams:any, allowedRoles:any[]):Promise<boolean>{
  // Check if authenticated user as any of the roles in array 'allowedRoles'
  return new Promise((resolve, reject) => {
    // If one of allowed roles is root, check against global variables
    if(allowedRoles.indexOf('root')>=0 && 
        ( root_auth.email.localeCompare(authParams.email)===0 || 
          root_auth.uid.localeCompare(authParams.uid)===0)){
      resolve(true);
    }
    // Get autID
    const uid = authParams.uid;
    // Get corresponding user in collection roles
    admin.firestore().collection('userRoles').doc(uid).get().then(snap => {
      // Get roles of user and compare against all roles in array 'allowedRoles'
      const memRoles  = snap.data().roles;
      let   found     = false;
      const   zz        = memRoles.length;
      for(let z=0; z<zz; z++){
        if(allowedRoles.indexOf(memRoles[z])){
          found = true;
          break;
        }
      }
      if(found)
        resolve(true);
      else{
        reject({message: 'Permission denied'});
      }
    }).catch(err => {
      reject(err);
    });
  });
}


/** Apply filters to list of members
 * ==================================
 * Following the parameters received from the request, get 
 * the maximum number of members respecting the constraints.
 * Returns a reference to the query */
function membersFiltersApplied(stopValue, direction, limit){
  if(stopValue===undefined || stopValue===null){
    if(direction===1)
      return fsDB.collection('members_header').orderBy('fullName').limit(limit);
    return fsDB.collection('members_header').orderBy('fullName', 'desc').limit(limit);
  }
  if(direction===1)
    return fsDB.collection('members_header').orderBy('fullName').startAfter(stopValue).limit(limit);
  return fsDB.collection('members_header').orderBy('fullName').endBefore(stopValue).limit(limit);
}


/** Callback to filter next batch of members until reaching the required limit, or the maximum 
 * ============================================================================================ */
function filterMembersCallback(conf, members, direction, stopValue){
  const pLimit      = conf.range.limit - members.length;
  // const pStopValue  = (members.length>0)? members[members.length-1].fullName:undefined;
  let pStopValue = stopValue;
  return membersFiltersApplied(pStopValue, direction, pLimit).get().then((snapshot) => {
    // snapshot is supposed to be the (T-n) next members validating the constraints.
    // Sort this put to see what to keep 
    let checkGender = false;
    if(conf.filter.gender!==undefined && conf.filter.gender!==null)
      checkGender = true;
    let  checkActive = false;
    if(conf.filter.active!==undefined && conf.filter.active!==null)
      checkActive = true;
      
    snapshot.forEach( elt => {
      const val   = elt.data();
      val.uid     = elt.id;
      pStopValue  = val.fullName;
      if(checkGender && val.gender.toUpperCase().localeCompare(conf.filter.gender)!==0)  return;
      if(checkActive && val.active!==conf.filter.active) return;
      members.push(val);
    });
    
    if(members.length>=conf.range.limit || snapshot.size<pLimit ){
      return members;
    } else {
      return filterMembersCallback(conf, members, direction, pStopValue).then(res => {
        return members;
      }).catch(err => {
        return err;
      });
    }
  }).catch( err =>{
    return err;
  });
}


/** Callback to get a valid participant ID
 * ======================================== */
function getValidParticipantID(item:any, counter:number):Promise<any>{
  const docID = item.uid + ((counter<10)?'_0':'_') + counter;
  const dbRef = fsDB.collection('participants').doc(item.semester);
  if(!item.create)
    return new Promise<any>((resolve, reject) => {
      resolve(dbRef.collection(''+item.amount).doc(item.uid));
    })
  return dbRef.collection( ''+item.amount).doc(docID).get().then(docRes => {
    if(!docRes.exists){ // If not exists, return that id as valid
      const validRef = dbRef.collection( ''+item.amount).doc(docID);
      return new Promise<any>((resolve, reject) => { resolve(validRef)  });
    } else { // If exists, try again with another number tag
      return getValidParticipantID(item, counter+1);
    }
  }).catch(err => {
    return new Promise<any>((resolve, reject) => { reject(err); })
  });
}

/** FETCH CHARGES
 * ============== */
function fetchCharges(data){
  const chargeRef        = fsDB.collection('charges');
  let   allCharges:any[] = [];
  let   requestIDs:Promise<any>;

  if(data.full!==true && data.allMembers===true){ // Only totals needed, get all active members first
    const conf = {
      range  : { limit:  10000, },
      filter : { active: true,  }
    }
    let allMembers:any[] = [];
    requestIDs = filterMembersCallback(conf, allMembers, 1, undefined);
  } else {// Get member uid from request
    requestIDs = new Promise<any>((resolve, reject) => { resolve(data.member)});
  }

  return requestIDs.then(ids => {
    let requestDetails : Promise<any>;
    let allIDs:any[] = [];
    // If request is list of all members, extract only ids
    if(data.full!==true && data.allMembers===true){
      ids.forEach(element => {
        allIDs.push(element.id);
      });
    } else {
      allIDs = ids;
    }
    if(data.full!==true){ // Request only totals
      requestDetails = chargeRef.get();
    } else {
      requestDetails = chargeRef.doc(allIDs[0]).collection('allcharges').where("balance", ">", 0).get();
    }
    return requestDetails.then(snapshot => {
      allCharges = [];
      if(data.full!==true && snapshot!==undefined){ // Only totals requested
        snapshot.forEach(snap => {
          const singleItem = {
            total:  snap.get('total'),
            uid:    snap.id
          };
          allCharges.push(singleItem);
        });
      } else { // Request full details
        snapshot.forEach(snap => {
          const singleItem = snap.data();
          singleItem.uid = snap.id;
          allCharges.push(singleItem);
        });
      }
      return allCharges;
    }).catch(err => {
      return err;
    });
  });
}

/** FETCH BANK DETAILS
 * =================== */
function fetchBankDetails(data){
  const bankRef        = fsDB.collection('bank').doc(data.year);
  let   allItems:any[] = [];
  let   request:Promise<any>;

  if(data.full===true){ // If full, retrieve only all transactions of the member specified
    request = bankRef.collection('transactions').doc(data.members[0]).collection('transactions').get();
  } else {            // Get total of all members
    request = bankRef.collection('transactions').get();
  }

  return request.then(snapshots => {
    if(data.full){
      snapshots.forEach(snap => {
        const item = snap.data();
        item.uid = snap.id;
        allItems.push(item)
      });
    } else {
      snapshots.forEach(snap =>{
        const item:any = { total: snap.data().total, member: snap.id};
        allItems.push(item);
      })
    }
    return allItems;
  }).catch(err => {  return err; })
}
